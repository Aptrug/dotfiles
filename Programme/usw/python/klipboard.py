#!/usr/bin/env python

import pyperclip

while True:
    with open("/home/pactrag/.cache/clipboard_history.txt", "a") as klipboard:
        klipboard.write("\n")
    with open("/home/pactrag/.cache/clipboard_history.txt", "a") as klipboard:
        klipboard.write(pyperclip.waitForNewPaste())
    with open("/home/pactrag/.cache/clipboard_history.txt", "a") as klipboard:
        klipboard.write("\n")
    with open("/home/pactrag/.cache/clipboard_history.txt", "a") as klipboard:
        klipboard.write("#" * 80)
