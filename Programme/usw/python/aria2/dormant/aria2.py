#!/usr/bin/env python

import time
import requests

# taken from https://tinyurl.com/yyvgl2f2


def aria2_request(jsonrpc):
    headers = {
        "Content-Type": "application/json",
    }
    url = "http://localhost:6800/jsonrpc"
    r = requests.post(url, headers=headers, json=jsonrpc)
    return r.json()


def get_jsonrpc(method):
    return {"id": "aria", "method": f"aria2.{method}"}


def convert_item(item):
    totalLength = int(item["totalLength"])
    completedLength = int(item["completedLength"])
    # if netspeed is not needed, commment the next line
    print(int(item["downloadSpeed"]) / 1000, "KB/s", end=" | ")
    return completedLength / totalLength * 100


while True:
    jsonrpc = get_jsonrpc("tellActive")
    tellActive = aria2_request(jsonrpc)["result"]
    var = [convert_item(tellActive[0])]
    print("{:.2f}".format(var[0]), "%", sep=" ")
    time.sleep(1)
