#!/bin/sh

# # update neovim plugins
# # if mindepth 1 is not used, exec will try operating on `start`
# # which is not a git repository
# find /home/pactrag/.local/share/nvim/site/pack/default/start \
# 	-mindepth 1 -maxdepth 1 -type d -exec git --git-dir={}/.git \
# 	--work-tree={} pull origin master --rebase \+

# LS_COLORS
# run it when you know LS_COLORS has been updated
curl https://raw.githubusercontent.com/trapd00r/LS_COLORS/master/LS_COLORS \
	--output - | dircolors --sh - >/home/pactrag/.config/LS_COLORS

# # bashtop
# bashtopPath=/home/pactrag/Programme/usw/shell/inpath/no_arg//bashtop
# curl \
# 	--remote-name \
# 	https://raw.githubusercontent.com/aristocratos/bashtop/master/bashtop \
# 	--output-dir /home/pactrag/Programme/usw/shell/inpath/no_arg/
# ! [ -x "${bashtopPath}" ] && chmod +x "${bashtopPath}"
# unset bashtopPath

# vim-nix
# curl \
# 	--remote-name \
# 	https://raw.githubusercontent.com/LnL7/vim-nix/master/syntax/nix.vim \
# 	--output-dir /home/pactrag/.config/nvim/after/syntax/
