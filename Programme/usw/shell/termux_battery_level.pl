#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

my $ssh_cmd = <<~'EOF';
/usr/bin/ssh -C -q -c aes128-ctr user@192.168.1.173 -p 8022 "
    /system/bin/su -c '
        /system/bin/cat /sys/class/power_supply/battery/capacity
        /system/bin/cat /sys/class/power_supply/battery/status
    '
"
EOF

my $battery_information;

open my $PH, '-|', $ssh_cmd || return 0;
chop( my $capacity = <$PH> );
if ($capacity) {
    my $status = <$PH> eq 'Charging' ? '+' : '-';
    $battery_information = "$status$capacity%";
}
else {
    $battery_information = 'is not working';
}
close $PH;

my $dunstify_cmd = <<~"EOF";
DBUS_SESSION_BUS_ADDRESS='unix:path=/run/user/1000/bus' /usr/bin/dunstify \\
--urgency low --timeout=2000 "Termux $battery_information"
EOF

exec $dunstify_cmd;
