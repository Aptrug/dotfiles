#!/bin/sh

set -efu

rsync() {
	# tar is better than scp since it it is conscious about the paths of the
	# files (will create subdirectories if they do not exist, as long as
	# depth 0 destination exists). Thus we will make `find` only process
	# `-type f` since `tar` will take care of the directories. On top of this,
	# `tar` will actually compress the files so that they will transmitted in
	# a faster manner on a slow connection. scp also supports compression
	# using `-C`, but it only enables gzip compress (.gz) which is way less
	# efficient that tar.xz size-wise

	localDir="$1"
	remoteDir="$2"

	# rsync compares the checksum, but calculating checksum is generally
	# slower than calculating the size and `find` has no builtin flag to check
	# checksums anyway, hence I will compare local and remote files based on
	# their size. And no, comparing modification date of files make no sense
	# in this case.

	localFiles="$(
		find "${localDir}" -type f -printf '%P\t%s\n'
	)"

	remoteFiles="$(
		ssh -q -c aes128-ctr user@"${TERMUX-}" -p 8022 "
find ${remoteDir} -type f -printf '%P\t%s\n'
"
	)"

	eval "$(
		awk '
BEGIN {
	ORS = " "
	FS = "\t"
	# initialize unlink array otherwise awk will complain whenever it is
	# empty
	split("", unlink, "")
	arrLength = 0
	unlinkLength = 0
}

{
	# in case $localFiles are being processed
	if (NR == FNR) {
		arr[$1] = $2
		++arrLength

	# check if there any files that are in $remoteDir and are not in
	# $localDir and add them to an array (they will be removed later)
	} else if (!($1 in arr)) {
		unlink[$1] = ""
		++unlinkLength
	} else if (arr[$1] == $2) {

		# remove elements (files) that have the same size both in $localDir and
		# $removeDir and keep only elements that have different size
		delete arr[$1]
		--arrLength
	}
}

END {

# If all files are identical, exit or else `tar` will complain about
# having 0 file argument. I know, no files will be removed if they
# exist on $remoteDir and not on $localDir, but that is no problem, there
# is no rush in deleting them.
if (arrLength == 0) {
	exit
}

# --directory=dir flag is similar to `cd dir && tar ...`
# Why do we need `--directory` flag? You might ask; well, the thing is, tar will
# create the archive but it will use the absolute paths of the files which  will
# be a problem when extracting since tar will actually extract
# /home/pactrag/Programme/usw/shell/inpath/ariaProgress instead of ariaProgress
# we do not want that to happen. Also, do not be confused by tar
# commands, I just used the long arguments instead of the shorter ones
# XZ_OPT=-9 is the maximum level of compression for tar.xz, there is also
# XZ_OPT=-9e (e stands for extreme), but it is rarely faster -9 and can
# sometimes be slower, so I will steer away from it
print "XZ_OPT=-9 tar --create --xz --file=- --directory='"${localDir}"'"

for (i in arr){
	# remember that we used `-printf "%P\t%s\n"` with `find`, we have to only
	# keep %P
	sub(/[[:space:]].*/, "", i)

	# this will print `ariaProgress` instead of the absolute path of file or
	# `./ariaProgress`, this is no problem since tar is already in the right
	# working directory: $localDir
	print "\47" i "\47"
}
print "| ssh -c aes128-ctr -p 8022 user@'"${TERMUX-}"' \"tar --extract",
"--xz --file=- --directory='"${remoteDir}"';"

if (unlinkLength != 0){
	print "rm"
	for (i in unlink){
		i = "'"${remoteDir}"'/" i
		print "\047" i "\047"
	}
}
print "\"\n"
}
' /dev/fd/3 /dev/fd/4 3<<-EOF 4<<-EOF
			${localFiles}
		EOF
			${remoteFiles}
		EOF
	)"
}

# no need to redirect the output to /dev/null since since tar is always silent
# unless there is an error or when --verbose flag is passed
rsync /home/pactrag/Programme/usw/shell/inpath \
	/data/data/com.termux/files/home/scripts/pactrag

rsync /home/pactrag/.config/nvim/ \
	/data/data/com.termux/files/home/.config/nvim


connect() {
	if [ "$(adb devices | wc --lines)" -gt 2 ]; then
		adb forward tcp:8022 tcp:8022
		ssh localhost -p 8022
	else
		ssh -c aes128-ctr -p 8022 user@"${TERMUX-}"
	fi
}

# No need for adb.
connect
# ssh -c aes128-ctr -p 8022 user@"${TERMUX-}"

# rebundant rsync
# # --compress is z flag
# # --delete deletes files from termux if they do not existing in nixos
# rsync_destination=/data/data/com.termux/files/home/scripts/nixos
# rsync --archive --delete --verbose --compress --progress --human-readable \
# 	--rsh='ssh -c aes128-ctr -p 8022' \
# 	/home/pactrag/Programme/usw/shell/inpath/ \
# 	user@"$TERMUX":"$rsync_destination" >/dev/null 2>&1 &
