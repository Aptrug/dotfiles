#!/usr/bin/gawk -f

# dmesg (too complicated and verbose)
# lsblk, TRAN,RM,MOUNTPOINT,PATH,REV

# The script is on par with usb_mount, just needs some testing, as I did not
# have any USB sticks around when I wrote this script.

BEGIN {
	LINT = "fatal"

	lsblk_cmd = "/usr/bin/lsblk --raw --output TRAN,MOUNTPOINT,PATH"

	no_usb_found = 1
	while((lsblk_cmd | getline) > 0) {
		if(!(/^usb/)) {
			continue
		}
		no_usb_found = 0
		lsblk_cmd | getline
		if (/^[[:space:]]{2}/){
			directory = $1
			sub(/\/dev\//, "", directory)
			directory = "/mnt/" directory
			where2mount[$1] = directory
		}
	}
	close(lsblk_cmd)

	if (no_usb_found) {
		print "no device to mount"
		exit
	}

	for (device in where2mount){
		sh_cmd = "/usr/bin/su -c 'test -d "  where2mount[device]\
		" || mkdir " where2mount[device] "; mount " device " "\
		where2mount[device] "'"
		system(sh_cmd)
	}
}
