#!/bin/sh

set -efu
PATH=/usr/bin
export DBUS_SESSION_BUS_ADDRESS='unix:path=/run/user/1000/bus'

notification_output() {
	awk '
	(NR == 2) {
		print $1 ":", $5, "(" $3 "/" $4 ")"
	}
	' <<-EOF
		$(df --human-readable --portability /dev/disk/by-label/arch_linux)
	EOF
}

dunstify --urgency low --timeout=3000 "$(notification_output)"
