#!/bin/sh

/usr/bin/pacman --sync \
	--downloadonly --noprogressbar --noconfirm --quiet --refresh --sysupgrade
updates_number="$(pacman --query --quiet --upgrades | wc --lines)"
if [ "${updates_number}" -ne 0 ]; then

	# Dunst never runs as root, therefore we must resort to su with
	# substitute user (pactrag).
	/usr/bin/su pactrag -c "
	DBUS_SESSION_BUS_ADDRESS='unix:path=/run/user/1000/bus' /usr/bin/dunstify \
		--urgency low --timeout=10000 '
	${updates_number} updates available
	'
	"
fi
