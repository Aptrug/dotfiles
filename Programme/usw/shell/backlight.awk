#!/usr/bin/gawk -f

function usage()
{
	print "Usage: [+/-][PERCENTAGE]\nExample: `backlight +20'"
	exit
}

BEGIN {
	LINT = "fatal"

	max_brightness_file = "/sys/class/backlight/intel_backlight/max_brightness"
	getline < max_brightness_file
	max_brightness = $0
	close(max_brightness_file)

	brightness_file = "/sys/class/backlight/intel_backlight/brightness"
	getline < brightness_file
	brightness = $0
	close(brightness_file)

	if (ARGC != 2) usage()

	if (ARGV[1] !~ /^[+-][1-9][0-9]?0?$/) usage()

	signe = substr(ARGV[1], 1, 1) "1"
	percentage = substr(ARGV[1], 2)

	value = signe * percentage * (max_brightness / 100)
	brightness_new = brightness + value

	if (brightness_new <= 0) brightness_new = 0

	if (brightness_new > max_brightness) brightness_new = max_brightness

	printf "%d", brightness_new > brightness_file
	close(brightness_file)

	brightness_percentage = int((brightness_new / max_brightness) * 100)
	notification_command = "/usr/bin/dunstify --replace=97 --timeout=1000 " \
						 "--urgency low " brightness_percentage "%"
	system(notification_command)
}
