#!/bin/sh

set -efu

case "$1" in
clear)
	clear && tmux clear-history
	;;
history)
	fzf --no-sort --reverse </home/pactrag/.cache/bash_history |
		perl -e '
ioctl STDOUT, 0x5412, $_ for split //, do { chomp($_ = <>); $_ }; print "\n"
'
	# xdotool type --delay 0 "$(
	# 	fzf --no-sort --reverse </home/pactrag/.cache/bash_history
	# )"
	;;
clipboardHistory)
	tac /home/pactrag/.cache/clipboard_history.txt | fzf --no-sort --reverse |
		xsel -bi
	;;
*)
	printf 'Illegal option!\n'
	;;
esac
