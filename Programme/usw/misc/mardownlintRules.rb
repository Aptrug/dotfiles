# Enable all rules by default
all

# increase line length limit warning to 100
rule 'MD013', :line_length => 100
