#!/bin/sh

set -efu

# shellcheck disable=SC2039

# more familiar terminal
#PS1="\u@\h:\w\[$(tput sgr0)\]"
#export PS1

# SSH
termux-wake-lock
sshd

# tmux
if command -v tmux &>/dev/null && [ -z "$TMUX" ]; then
    tmux attach -t default || tmux new -s default
fi

# chmodding
chmod +x ~/Programme/usw/shell/*
chmod +x ~/Programme/usw/shell/termux/*
