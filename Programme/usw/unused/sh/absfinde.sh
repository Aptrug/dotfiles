#!/bin/sh

set -efu

printf "\e[1;33mVerzeichnisse \e[0m" &&
printf '\n' && find "$(pwd)" -type d -iname "*$1*" &&
printf "\e[1;33mDateien \e[0m" && printf '\n' &&
find "$(pwd)" -type f -iname "*$1*"
