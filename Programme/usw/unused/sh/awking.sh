#!/bin/sh

set -efu

set -e
set -u

while getopts 'lha:' OPTION; do
	case "$OPTION" in
	l)
		echo "linuxconfig"
		;;

	h)
		echo "h stands for h"
		;;

	a)
		# avalue="$OPTARG"
		echo "$OPTARG"
		echo "$OPTION"
		echo "$OPTERR"
		echo "$OPTIND"
		# unbound
		# echo "$OPTOPT"
		# echo "$OPTION"
		;;
	?)
		echo "script usage: $(basename "$0") [-l] [-h] [-a somevalue]" >&2
		exit 1
		;;
	esac
done
# shift "$(($OPTIND - 1))"
