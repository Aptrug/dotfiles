#!/bin/sh

set -efu

shellScript="$1"
awkOccurances="$(grep -c "awk '$" "$1")"
printf '%s\n' "$awkOccurances"

# awkScript=/tmp/awkScript.pl

i=0 && while [ $i -le "$awkOccurances" ]; do
	i=$((i + 1))

	sed -i "0,/awk '$/ s//finibusElementum/" "$shellScript"
	indentation="$(grep 'finibusElementum' "$shellScript" | grep -Eo '^\s+')"
	formattedAWK="$(awk '
	/finibusElementum/ {
		isAWKScript = 1
		next
	}

	# match `" `, `"$` or ``|" (in case no space exists between " and a pipe)
	# /'\''\s|'\''$|'\''\|/ {
	/\s+'\''/ {
		isAWKScript = 0
	}

	isAWKScript{
		print
	}
' "$shellScript" | awk -f- -o- | sed "s/^/$indentation/")"

	# check if string contains single quotes
	sed -i '/finibusElementum/,/'\''[^\\]/{//!d}' "$shellScript"

	sed -i -e "
	/finibusElementum/ r /dev/stdin
	0,/finibusElementum/s//elementumFinibus/
	" "$shellScript" <<-EOF
		$formattedAWK
	EOF
done

sed -i "s/elementumFinibus/awk '/" "$shellScript"
