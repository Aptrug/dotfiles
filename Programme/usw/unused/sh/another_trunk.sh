#!/bin/sh

set -efu

# I will keeping using `ls` despite errors, since only `ls` can group directories
clear && ls \
	--dereference `# for handling the symlinks` \
	--almost-all `# list everything except "." and ".."`\
	--indicator-style=slash `# important — will come in use later` \
	--group-directories-first `# the main reason I skipped "find"`|
	sed '
	# match files only (recognizable due to `-p` flag of `ls`)
	/\/$/! {
		# color them by executing `ls`
		s/^/ls --color=always /e
		# skip the line if not a file
	}
	/\/$/ {
		# duplicate the line, since the original will be soon overwritten
		p
		# execute `ls` to show the contents of the directory
		s/^/ls --color=always /e
		# match directories that are not empty
		/^$/!{
			# add the indentation for the first line (no `g` needed here)
			s/^/├───────── /
			# and for the rest of the lines
			s/\(\n\)/\1├───────── /g
			# b is like "else"
			b
		}
		/^$/{
			# else delete the line (if the directory is empty)
			d
		}
	}
	'
