#!/bin/sh

set -efu

tmp1="$(mktemp)"
tmp2="$(mktemp)"

trap 'rm -f "$tmp1"; rm -rf "$tmp2"' EXIT

source=/home/pactrag/Downloads/

cd "$source" || exit

# send all .aria2 file to tmp1
find . -type f -name \*.aria2 >"$tmp1"

#sed -i -e 's: :\\ :g' "$tmp2"

# clone the same file
cp "$tmp1" "$tmp2"

#sed -i -e 's/\.aria2$//' -e 's: :\\ :g' "$tmp2"

# change e.g. video.mkv.aria2 to video.mkv
sed -i -e 's/\.aria2$//' "$tmp2"

rsync "$source" --progress \
    --archive \
    --verbose \
    --recursive \
    --ignore-times \
    --remove-source-files \
    --files-from="$tmp2" /tmp/recycle_bin
rsync "$source" --progress \
    --archive \
    --verbose \
    --recursive \
    --ignore-times \
    --remove-source-files \
    --files-from="$tmp1" /tmp/recycle_bin

find . -maxdepth 1 -type d -empty -delete
