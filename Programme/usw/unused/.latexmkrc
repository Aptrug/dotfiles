$postscript_mode = $dvi_mode = 0;

#
#$pdflatex = ' -lualatex      interaction=nonstopmode --shell-escape -synctex=1';
$out_dir = '/tmp/';
#$pdflatex = 'lualatex -synctex=1 -file-line-error -halt-on-error -interaction=nonstopmode --shell-escape %O %S && cp "%D" "%R.pdf"';
$pdflatex = 'lualatex -synctex=1 -file-line-error -quiet -halt-on-error-interaction=nonstopmode --shell-escape %O %S && cp "%D" "%R.pdf"';
$pdf_mode = 1;
$preview_continuous_mode = 1;
#$bibtex_use = 2;
$silent = 1;
#$pdf_previewer="start okular %O %S";

#--------------------------------------------------------------------------------------------------------------------------------------------
