set ts=4 sts=4 sw=4 tw=80 et ai
set rnu undofile sm hls scs ignorecase lbr spr sb 
filetype plugin indent on
let python_highlight_all=1
syntax on
noremap <F5> <Esc>:w<Esc>:only!<CR>:vert ter python3 %<CR>
