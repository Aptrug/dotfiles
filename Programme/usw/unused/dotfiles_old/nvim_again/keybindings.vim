" Remapping leader to spacebar
let mapleader = "\<Space>"
let maplocalleader = ';'

" disabling spacebar in normal mode
nnoremap <Space> <NOP>
nnoremap <S-Space> <NOP>

" gm to go in the half of current line (replaces defaults)
map gm :call cursor(0, virtcol('$')/2)<CR>

" keyboard shortcuts to certain files
nnoremap gev :tabnew $MYVIMRC<CR>
nnoremap gsv :source $MYVIMRC<CR>
nnoremap gfv :tabnew /home/pactrag/.config/nvim/configs/functions.vim<CR>
nnoremap gmv :tabnew /home/pactrag/.config/nvim/configs/main.vim<CR>
nnoremap gpv :tabnew /home/pactrag/.config/nvim/configs/plugins.vim<CR>
nnoremap gkv :tabnew /home/pactrag/.config/nvim/configs/keybindings.vim<CR>

"keep visual mode after indent
vnoremap > >gv
vnoremap < <gv
"vnoremap y ygv

" <leader>r to compile and run
noremap <leader>r :call CompileRunGcc()<CR>

" move up/down regardless of line length
nnoremap <Down> gj
nnoremap <Up> gk

" make c cut
nnoremap cc "_dd
vnoremap c "_d
vnoremap c "_d
nnoremap c "_d
nnoremap C "_D
" make remove actually remove
nnoremap x "_x
nnoremap X "_X
vnoremap x "_x
vnoremap X "_X
nnoremap s "_s
nnoremap S "_S
vnoremap s "_s
vnoremap S "_S
"nnoremap d "_d
"vnoremap d "_d
"nnoremap D "_D
"vnoremap D "_D
"nnoremap dd "_dd

" make paste in visual mode not overwrite the last yank
"vnoremap p "0p

" remap pgup and pgdown to ctrl-e ctrl-y
nnoremap <PageDown> <C-e>
nnoremap <PageUp> <C-y>

" <C-q> is unassigned in both Neovim and Emacs
" silent prevents 'Term_toggle(10)' from showing in the command line
nnoremap <silent> <C-q> :call Term_toggle(10)<cr>
tnoremap <silent> <C-q> <C-\><C-n>:call Term_toggle(10)<cr>

"""""""""""""""""""vim-unimpaired""""""""""""""""""""""""""""""
nnoremap <silent> [<space>  :<C-u>put!=repeat([''],v:count)<bar>']+1<cr>
nnoremap <silent> ]<space>  :<C-u>put =repeat([''],v:count)<bar>'[-1<cr>

"""""" function
nnoremap <silent> <F1> :call ToggleList("Location List", 'l')<CR>
nnoremap <silent> <F2> :call ToggleList("Quickfix List", 'c')<CR>

nnoremap "! :cal SetBang(0)<cr>
xnoremap "! :cal SetBang(1)<cr>
