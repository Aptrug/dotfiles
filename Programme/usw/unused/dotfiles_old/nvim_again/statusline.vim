function! FileSize(bytes)
	let l:bytes = a:bytes | let l:sizes = ['B', 'K', 'M', 'G'] | let l:i = 0
	while l:bytes >= 1024 | let l:bytes = l:bytes / 1024.0 | let l:i += 1 | endwhile
	return l:bytes > 0 ? printf('%.1f%s', l:bytes, l:sizes[l:i]) : ''
endfunction


" there is strlen(), strchars() and strwidth(), but only strdisplaywidth is capable
" of dealing with tabs
" show full path to the file as long as strchars(path) < 100
" set statusline=%-.100F\
"\ %m\

" function! ChangeStatuslineColor()
"   " if (mode() ==# 'i')
"     " exe 'hi! StatusLine ctermfg=yellow ctermbg=black'
"   if (mode() ==# '!')
"     exe 'hi! StatusLine ctermfg=cyan ctermbg=black'
"   else
"     exe 'hi! StatusLine ctermfg=white ctermbg=black'
"   endif
"   return ''
" endfunction
" set statusline+=%{ChangeStatuslineColor()}

set statusline=file:\%f\
			\ \|\
			\ size:\%{FileSize(line2byte('$')+len(getline('$')))}\
			\ \|\
			\ type:\%Y\
			\ \|\
			\ buffNum:%{len(filter(range(1,bufnr('$')),'buflisted(v:val)'))}\
			\ %#warningmsg#%r%*\
			\ %=\
			\ %#warningmsg#%{&modified?'Modified':''}%*\
			\ lastSaved:\%{strftime('%Y%m%dT%H%M%S',getftime(expand('%:p')))}\
			\ \|\
			"\ this is a comment
			\ problems:\%{ale#statusline#Count(bufnr(''))['total']}\
			\ \|\
			\ percentage:\%p%%\
			\ \|\
			\ column:\%v/%{strdisplaywidth(getline('.'))}\
			\ \|\
			\ line:\%l/%L

" highlight statusline ctermfg=yellow ctermbg=black
"
" hi InsertColor ctermbg=yellow ctermfg=black
