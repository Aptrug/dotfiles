"----------ale----------

let g:ale_fix_on_save = 1
let g:ale_fixers = ['remove_trailing_lines', 'trim_whitespace']

let g:ale_echo_msg_format = '%severity% [%linter%] (%code%) - %s'
