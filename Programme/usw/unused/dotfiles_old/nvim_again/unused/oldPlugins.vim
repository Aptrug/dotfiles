" integrate coc with ale; should be before plugins are loaded
"let g:ale_disable_lsp = 1

call plug#begin('~/.vim/plugged')
Plug 'preservim/nerdcommenter'
Plug 'LnL7/vim-nix' " needed for nix syntax highlighting
call plug#end()


"----------ale----------

"let g:ale_list_vertical = 1
"let g:ale_list_window_size = 95
"let g:ale_list_window_size = 70
let g:ale_fix_on_save = 1
"let g:ale_set_quickfix = 1

" show error messages
"let g:ale_open_list = 1

let g:ale_linters = {
            \ 'vim': ['vint', 'vimls'],
            \ 'python': ['flake8'],
            \ 'sql': ['sqlint', 'sqllint'],
            \ 'nix': ['nix-instantiate'],
            \ 'sh': ['shellcheck', 'shell', 'language_server'],
            \ 'perl': ['perl', 'perlcritic'],
            \ 'markdown': ['mdl', 'languagetool'],
            \ 'java': ['checkstyle',
            \ 'eclipselsp',
            \ 'javac',
            \ 'pmd'],
            \ 'cpp': ['cc',
            \ 'ccls',
            \ 'clangd',
            \ 'clangtidy',
            \ 'cppcheck',
            \ 'cc',
            \ 'cpplint'],
            \ 'c': ['cc',
            \ 'ccls',
            \ 'clangd',
            \ 'clangtidy',
            \ 'cppcheck',
            \ 'cc',
            \ 'cpplint'],
            \ 'go': ['golangci-lint',
            \ 'gopls',
            \ 'staticcheck',
            \ 'golangserver',
            \ 'gotype',
            \ 'gobuild',
            \ 'govet',
            \ 'gofmt',
            \ 'golint'],
            \ 'rust': ['analyzer',
            \ 'cargo',
            \ 'rls',
            \ 'rustc']
            \}
"'clangcheck',
let g:ale_fixers = {
            \ '*': ['remove_trailing_lines', 'trim_whitespace'],
            \ 'tex': ['latexindent'],
            \ 'sh': ['shfmt'],
            \ 'perl': ['perltidy'],
            \ 'nix': ['nixpkgs-fmt'],
            \ 'python': ['black'],
            \ 'sql': ['pgformater'],
            \ 'cpp': ['clang-format', 'clangtidy'],
            \ 'rust': ['rustfmt'],
            \ 'go': ['gofmt', 'goimports'],
            \ 'java': ['google-java-format'],
            \ 'c': ['clang-format', 'clangtidy']
            \}

"let g:ale_fix_on_save_ignore = {
            "\      'cpp': ['uncrustify', 'astyle'],
            "\      }

"--------------------------------------------------
" yarn global add bash-language-server markdownlint-cli vim-language-server
" sudo zypper in 'perl(Log::Log4perl)' 'perl(YAML::Tiny)' \
" 'perl(File::HomeDir)' 'perl(Unicode::GCString)'

"------------------------------------------
            "\ 'python': ['add_blank_lines_for_python_control_statements'],

" " linters
" vint / python3-vim-vint: Main Repository
" vimls / coc-vimlsp: yarn package
" flake8 / python3-flake8: Main repository
" mdl: rubygem (as root)
" markdownlint: yarn
" remark-lint: yarn
"""recommended:
"sudo zypper in python3-flake8-blind-except python3-flake8-bugbear \
"python3-flake8-builtins python3-flake8-class-newline \
"python3-flake8-comprehensions python3-flake8-debugger python3-flake8-deprecated \
"python3-flake8-import-order \
"python3-flake8-pep3101 \
"python3-flake8-polyfill python3-flake8-quotes python3-flake8-pyi
" shellcheck / ShellCheck: Main Repository
" bashate / python3-bashate: Main Repository
" shell / shell: **no requirements**
" language_server / bash-language-server: yarn package
" " fixers
" shfmt / shfmt: yarn
" black/ python3-black: Main repository

" show linter name on quickfix

"""ale options"""
let g:ale_echo_msg_format = '%severity% [%linter%] (%code%) - %s'
let g:ale_cpp_cpplint_options = '--filter=-legal/copyright'
let g:ale_python_black_options = '--line-length 100'
let g:ale_tex_latexindent_options = '-c=/tmp/texmp'
let g:ale_c_cc_options = '-Weverything'
let g:ale_sh_bashate_options = '--max-line-length 100'
let g:ale_python_flake8_options = '--max-line-length 100'
let g:ale_markdown_mdl_options = '--style /home/pactrag/Programme/usw/misc/mardownlintRules.rb'


" ale executable
let g:ale_java_eclipselsp_path = '/home/pactrag/Programme/binaries/eclipse.jdt.ls-0.67.0'
"let g:ale_markdown_mdl_executable = '/usr/bin/mdl.ruby2.7'
"let g:ale_languagetool_executable = '/home/linuxbrew/.linuxbrew/Cellar/languagetool/5.0/libexec/languagetool-commandline.jar'
"let g:ale_languagetool_executable = '/home/linuxbrew/.linuxbrew/Cellar/languagetool/5.0/libexec/languagetool-commandline.jar'
"let g:tex_textidote_options = '--read-all --quiet --output singleline --check de'
"let g:tex_textidote_check_lang = 'de'
"let g:tex_textidote_executable = '/home/pactrag/Programme/Scripts/textidote'
"let g:ale_alex_executable = '/home/pactrag/.local/share/node/node_modules/alex'

" " navigate error messages
nmap <silent> ]c :ALENext<cr>
nmap <silent> [c :ALEPrevious<cr>
"nmap <silent> <C-k> <Plug>(ale_previous_wrap)
"nmap <silent> <C-j> <Plug>(ale_next_wrap)

"augroup set-src-as-filetype
"au BufRead,BufNewFile *.src set filetype=sh
"augroup end

"----------vimtex----------
"" spellchecking
"augroup latex-spellchecking
"au BufRead,BufNewFile *.en.tex setlocal spell spelllang=en_gb
"au BufRead,BufNewFile *.fr.tex setlocal spell spelllang=fr
"au BufRead,BufNewFile *.de.tex setlocal spell spelllang=de
"au BufRead,BufNewFile *.en.fr.tex setlocal spell spelllang=en,fr
"augroup end

"augroup textidote-spellchecking
"au BufRead,BufNewFile *.en.tex let b:tex_textidote_check_lang = 'en_uk'
"au BufRead,BufNewFile *.fr.tex let b:tex_textidote_check_lang = 'fr'
"au BufRead,BufNewFile *.de.tex let b:tex_textidote_check_lang = 'de_de'
"augroup end

let g:tex_flavor = 'latex'
"let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_general_viewer = 'zathura'

"let g:ale_tex_chktex_options = '-n 18'


"let g:vimtex_compiler_latexmk = {
            "\ 'build_dir' : '',
            "\ 'callback' : 1,
            "\ 'continuous' : 1,
            "\ 'executable' : 'latexmk',
            "\ 'hooks' : [],
            "\ 'options' : [
            "\ '-verbose',
            "\ '-pdflatex="lualatex --shell-escape %O %S && cp "%D" "%R.pdf""',
            "\ '-outdir=/tmp/latex',
            "\ '-file-line-error',
            "\ '-synctex=1',
            "\ '-halt-on-error',
            "\ '-interaction=nonstopmode',
            "\ ],
"\}
"let g:vimtex_format_enabled = 1
"----------vim-airline----------

let g:airline_section_x = "%{strwidth(getline('.'))}"
"let g:airline#extensions#ale#enabled = 1
"let airline#extensions#ale#error_symbol = 'E:'
"let airline#extensions#ale#warning_symbol = 'W:'
"let airline#extensions#ale#show_line_numbers = 1
"let airline#extensions#ale#open_lnum_symbol = '(L'
"let airline#extensions#ale#close_lnum_symbol = ')'


"----------nerdcommenter----------

" map cd <plug>NERDCommenterToggle
nmap <C-_> <Plug>NERDCommenterToggle
vmap <C-_> <Plug>NERDCommenterToggle<CR>gv

let g:NERDCustomDelimiters = {
            \ 'vimwiki': { 'left': '<!--', 'right': '-->' },
            \ '': { 'left': '# ', 'leftAlt': '#' }
            \ }

"----------coc.nvim----------

"    \ 'coc-sh',
"let g:coc_global_extensions = [
            "\ 'coc-vimtex'
            "\ ]

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
"inoremap <silent><expr> <TAB>
            "\ pumvisible() ? "\<C-n>" :
            "\ <SID>check_back_space() ? "\<TAB>" :
            "\ coc#refresh()
"inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

"function! s:check_back_space() abort
"let col = col('.') - 1
"return !col || getline('.')[col - 1]  =~# '\s'
"endfunction

"----------fzf.vim----------

"command! -bang -nargs=? -complete=dir Files
            "\ call fzf#vim#files(<q-args>, fzf#vim#with_preview(), <bang>0)
"nmap <leader>ff :Files<CR>
"nmap <leader>fm :Marks<CR>
"nmap <leader>fw :Windows<CR>
"nmap <leader>fb :Buffers<CR>
"nmap <leader>fh :History<CR>
"nmap <leader>fg :Tags<CR>
"nmap <leader>ft :BTags<CR>
"nmap <Leader>fc :FZF ~/.config<CR>
"nmap <Leader>fh :FZF ~<CR>
"nmap <Leader>fs :FZF ~/.bashsrc<CR>

"----------vim-cutlass----------

"nnoremap m d
"xnoremap m d
"nnoremap mm dd

"----------markdown-preview.nvim----------

let g:mkdp_browser = 'qutebrowser'

"----------vimwiki----------

augroup set-vimwiki-as-filetype
    au BufRead,BufNewFile *.wiki set filetype=markdown
augroup end

"nnoremap <C-\> :VimwikiToggleListItem<CR>
" <cr> is actually <enter>
"augroup latex-spellchecking
"autocmd! BufWritePost *.wiki exec "!/home/pactrag/Programme/Scripts/vimwiki-pusher.sh"
""autocmd! BufWritePost *.wiki silent! !git add ~/vimwiki ; git commit -a -m '' ; git push -u origin master
""autocmd! BufWritePost *.wiki !git add ~/vimwiki ; gitload
"augroup end

" indentation
augroup markdown-spacing
    autocmd BufNewFile,BufRead *.md,*.wiki setlocal tabstop=2 shiftwidth=2 softtabstop=2
augroup end

let g:vimwiki_list = [{
            \ 'syntax': 'markdown',
            \ 'ext': '.md',
            \ 'path': '~/verwirrt/notieren/vimwiki',
            \ 'links_space_char': '_'
            \}]

" assume all markdown files are vimwiki files
let g:vimwiki_global_ext = 0

" from markdown to pdf
augroup convert-markdown-to-pdf
    autocmd FileType markdown,vimwiki
                \ command! Pandoc w !clear;
                \ pandoc % --pdf-engine=xelatex -o ~/Downloads/Documents/Pandoc/$(basename % .md).pdf &&
                \ zathura $(basename % .md).pdf
    "au BufRead,BufNewFile *.md setlocal wrap
    "au BufRead,BufNewFile *.md setlocal textwidth=80
augroup end

"----------vim-startify----------
let g:startify_custom_header = 'startify#pad(startify#fortune#quote())'

"----------neoterm----------
let g:neoterm_autoinsert = 1

""Toggle the terminal with F10
nnoremap <silent> <F10> :vert Ttoggle<CR>
tnoremap <F10> <C-\><C-n>:Ttoggle<CR>

"----------vim-mucomplete----------
"imap <tab> <plug>(MUcompleteFwd)
