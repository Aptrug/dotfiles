function s:RestoreSession()
    if filereadable('./Sitzung.vim')
        source ./Sitzung.vim
    else
        echo 'No session file (Sitzung.vim) found'
    endif
endfunction


" Something is wrong with this function
" sourcing $MYVIMRC while it is uncommented outputs an error
" func! Unwrap_stuff()
"     setlocal textwidth=2139999999
"     normal! gggqG
"     setlocal textwidth=80
" endfunc
" command Unwrap exec 'call Unwrap_stuff()'


function! CompileRunGcc()
    write
	"e!
    vsplit
	" terminal /home/pactrag/Programme/usw/shell/inpath/kompile.sh %
	" bufdo e!
	terminal /home/pactrag/Programme/usw/shell/inpath/kompile.sh %
endfunc

function! AWKFixer()
	silent exec '!/home/pactrag/Programme/usw/shell/awkFixer.sh %'
	e!
endfunc

function! GetBufferList()
    redir =>buflist
    silent! ls!
    redir END
    return buflist
endfunction

function! ToggleList(bufname, pfx)
    let buflist = GetBufferList()
    for bufnum in map(filter(split(buflist, '\n'), 'v:val =~ "'.a:bufname.'"'),
				\'str2nr(matchstr(v:val, "\\d\\+"))')
        if bufwinnr(bufnum) != -1
            exec(a:pfx.'close')
            return
        endif
    endfor
    if a:pfx ==# 'l' && len(getloclist(0)) == 0
        echohl ErrorMsg
        echo 'Location List is Empty.'
        return
    endif
    let winnr = winnr()
    exec(a:pfx.'open')
    if winnr() != winnr
        wincmd p
    endif
endfunction

function! SetBang(v) range
        if a:v == 1
                normal gv
        endif
        let l:t = &shellredir
        let &shellredir = '>%s\ 2>/dev/tty'
        let @" = join(systemlist(input("\"!")),' ')
        let &shellredir = l:t
endf
