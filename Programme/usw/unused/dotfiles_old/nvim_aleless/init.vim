" Functions {{{1

function! Term_toggle(height)
	if win_gotoid(g:termWinVar)
		hide
	else
		vert new
		try
			exec 'buffer ' . g:term_buf
		catch
			call termopen($SHELL, {'detach': 0})
			let g:term_buf = bufnr('')
		endtry
		let g:termWinVar = win_getid()
	endif
endfunction

function! ToggleNetrw()
        let i = bufnr('$')
        let wasOpen = 0
        while (i >= 1)
            if (getbufvar(i, '&filetype') ==# 'netrw')
                silent exe 'bwipeout ' . i
                let wasOpen = 1
            endif
            let i-=1
        endwhile
    if !wasOpen
        silent Lexplore!
    endif
endfunction


function! FileSize(bytes)
	let l:bytes = a:bytes
	let l:sizes = ['B', 'K', 'M', 'G']
	let l:i = 0
	while l:bytes >= 1024
		let l:bytes = l:bytes / 1024.0
		let l:i += 1
	endwhile
	return l:bytes > 0 ? printf('%.1f%s', l:bytes, l:sizes[l:i]) : ''
endfunction

function s:RestoreSession()
    if filereadable('./Sitzung.vim')
        source ./Sitzung.vim
    else
        echo 'No session file (Sitzung.vim) found'
    endif
endfunction


" Something is wrong with this function
" sourcing $MYVIMRC while it is uncommented outputs an error
" func! Unwrap_stuff()
"     setlocal textwidth=2139999999
"     normal! gggqG
"     setlocal textwidth=80
" endfunc
" command Unwrap exec 'call Unwrap_stuff()'


function! CompileRunGcc()
    write
	"e!
    vsplit
	" terminal /home/pactrag/Programme/usw/shell/inpath/kompile.sh %
	" bufdo e!
	terminal /home/pactrag/Programme/usw/shell/inpath/kompile.sh %
endfunc

function! AWKFixer()
	silent exec '!/home/pactrag/Programme/usw/shell/awkFixer.sh %'
	e!
endfunc

function! GetBufferList()
    redir =>buflist
    silent! ls!
    redir END
    return buflist
endfunction

function! ToggleList(bufname, pfx)
    let buflist = GetBufferList()
    for bufnum in map(filter(split(buflist, '\n'), 'v:val =~ "'.a:bufname.'"'),
				\'str2nr(matchstr(v:val, "\\d\\+"))')
        if bufwinnr(bufnum) != -1
            exec(a:pfx.'close')
            return
        endif
    endfor
    if a:pfx ==# 'l' && len(getloclist(0)) == 0
        echohl ErrorMsg
        echo 'Location List is Empty.'
        return
    endif
    let winnr = winnr()
    exec(a:pfx.'open')
    if winnr() != winnr
        wincmd p
    endif
endfunction

" function! SetBang(v) range
"         if a:v == 1
"                 normal gv
"         endif
"         let l:t = &shellredir
"         let &shellredir = '>%s\ 2>/dev/tty'
"         let @" = join(systemlist(input("\"!")),' ')
"         let &shellredir = l:t
" endf

" Main {{{1

" VimEnter
" UIEnter
" UILeave
" TermResponse
" QuitPre
" ExitPre
" VimLeavePre
" VimLeave
" VimResume
" VimSuspend

" set formatoptions-=cro
augroup disable_auto_comment_insertion
	autocmd OptionSet * set formatoptions-=cro
augroup end

set clipboard+=unnamedplus
	\ textwidth=80
	\ colorcolumn=+0
	\ number
	\ undofile
	"\ this is a comment
	\ ignorecase
	\ smartcase
	"\ language-specific completion with C-x C-o
	\ omnifunc=syntaxcomplete#Complete
	\ showmatch
	\ cursorline
	\ noexpandtab
	\ tabstop=4
	\ softtabstop=4
	\ shiftwidth=4
	\ splitbelow
	\ splitright
	\ nojoinspaces
	\ directory=./
	\ autochdir
	\ inccommand=nosplit
	\ statusline=f:\%f\
		\ \|\
		"\ size
		\ s:\%{FileSize(line2byte('$')+len(getline('$')))}\
		\ \|\
		"\ t
		\ t:\%Y\
		\ \|\
		"\ buffNum
		\ b:%{len(filter(range(1,bufnr('$')),'buflisted(v:val)'))}\
		\ %#warningmsg#%r%*\
		\ %=\
		\ %#warningmsg#%{&modified?'Modified':''}%*\
		"\ last modification
		\ m:\%{strftime('%Y%m%dT%H%M%S',getftime(expand('%:p')))}\
		\ \|\
		"\ problems (ALE)
		"\ A:\%{ale#statusline#Count(bufnr(''))['total']}\
		\ \|\
		"\ percentage
		\ p:\%p%%\
		\ \|\
		"\ column
		\ c:\%v/%{strdisplaywidth(getline('.'))}\
		\ \|\
		\ line:\%l/%L

" opening help in new tab instead
cabbrev help tab help
" cabbrev helpgrep tab helpgrep

" make cursorline less intrusive
highlight CursorLine term=bold cterm=bold

" Fvck python
" let python_highlight_all = 1

function! RemoveTrailingSpaces()
    let l:winview = winsaveview()
    %s/\s\+$\|\_s*\%$//e
    call winrestview(l:winview)
endfunction
augroup removeAllTrailingWhitespaces
    autocmd BufWritePre * call RemoveTrailingSpaces()
augroup end

" show comments in italic
" set t_ZH=^[[3m
" set t_ZR=^[[23m

" disable annoying providers causing false alarms
let g:loaded_python_provider = 0
let g:loaded_ruby_provider = 0
let g:loaded_node_provider = 0

augroup no_number_terminal
	" TermOpen event is triggerd after opening the terminal window for the first
	" time, BufEnter event is triggered after unhiding the terminal window thus,
	" both are needed
	autocmd BufEnter,TermOpen term://* startinsert
augroup end

" I only edit markdown in the browser now
" augroup markdown_real_time_preview
" 	autocmd TextChanged,TextChangedI *.md silent write
" augroup end

augroup reload_file_on_change
	" every time nvim gains focus (supported by few terminals)
	" focusGained includes CmdlineLeave too
	autocmd FocusGained * checktime
augroup end
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Toggle the terminal
let g:termWinVar = 0 " a variable needed by Term_toggle()

" some netrw stuff
let g:netrw_liststyle=3     " tree view

" see changes since last save, taken from Neovim's own `help`, see
" `help last-position-jump`
command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
			\ | wincmd p | diffthis


" close quickfix window if it the last window
augroup QFClose
  " au!
  autocmd WinEnter * if winnr('$') == 1 && &buftype == "quickfix"|q|endif
augroup END

augroup hlsearchNoMore
	" hide the searched string when entering insert mode
	autocmd InsertEnter * :let b:_search=@/|let @/=''
	" requip the last searched string
	autocmd InsertLeave * :let @/=get(b:,'_search','')
augroup end

augroup rememberCursorPosition
    autocmd!
    autocmd BufReadPost *
        \ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
        \ | execute "normal! g`\"zvzz"
        \ | endif
augroup END


" Plugins {{{1

" let g:ale_fix_on_save = 1
" let g:ale_fixers = ['remove_trailing_lines', 'trim_whitespace']
" let g:ale_echo_msg_format = '%severity% [%linter%] (%code%) - %s'

" Keybindings {{{1

"""""""""""""""""""functions-dependending-keybindings""""""""""""""""""""""""""
nnoremap <silent> <F1> :call ToggleList("Location List", 'l')<CR>
nnoremap <silent> <F2> :call ToggleList("Quickfix List", 'c')<CR>

" hide highlighed search on hitting return
nnoremap <silent> <CR> :nohlsearch<CR><CR>

nnoremap "! :cal SetBang(0)<cr>
xnoremap "! :cal SetBang(1)<cr>

" <leader>r to compile and run
noremap <leader>r :call CompileRunGcc()<CR>

" <C-q> is unassigned in both Neovim and Emacs
" silent prevents 'Term_toggle(10)' from showing in the command line
nnoremap <silent> <F4> :call Term_toggle(10)<cr>
tnoremap <silent> <F4> <C-\><C-n>:call Term_toggle(10)<cr>

" toggle netrw
noremap <silent> <F3> :call ToggleNetrw() <CR>

"""""""""""""""""""non-functions-dependending-keybindings""""""""""""""""""""""

" Remapping leader to spacebar
let mapleader = "\<Space>"
let maplocalleader = ';'

" disabling spacebar in normal mode
nnoremap <Space> <NOP>
nnoremap <S-Space> <NOP>

" gm to go in the half of current line (replaces defaults)
map gm :call cursor(0, virtcol('$')/2)<CR>

" keyboard shortcuts to certain files
nnoremap gev :tabnew $MYVIMRC<CR>
nnoremap gsv :source $MYVIMRC<CR>
" nnoremap gfv :tabnew /home/pactrag/.config/nvim/configs/functions.vim<CR>
" nnoremap gmv :tabnew /home/pactrag/.config/nvim/configs/main.vim<CR>
" nnoremap gpv :tabnew /home/pactrag/.config/nvim/configs/plugins.vim<CR>
" nnoremap gkv :tabnew /home/pactrag/.config/nvim/configs/keybindings.vim<CR>

" quickly search for a flag in a man page
nnoremap <C-_> /^\s\+

"keep visual mode after indent
vnoremap > >gv
vnoremap < <gv
"vnoremap y ygv

" make c cut
nnoremap cc "_dd
vnoremap c "_d
vnoremap c "_d
nnoremap c "_d
nnoremap C "_D
" make remove actually remove
nnoremap x "_x
nnoremap X "_X
vnoremap x "_x
vnoremap X "_X
nnoremap s "_s
nnoremap S "_S
vnoremap s "_s
vnoremap S "_S
"nnoremap d "_d
"vnoremap d "_d
"nnoremap D "_D
"vnoremap D "_D
"nnoremap dd "_dd

" make paste in visual mode not overwrite the last yank
"vnoremap p "0p

" remap up arrow and down arrow to ctrl-e ctrl-y
nnoremap <Down> <C-e>
nnoremap <Up> <C-y>

" move up/down regardless of line length
nnoremap <C-j> gj
nnoremap <C-h> gk

"""vim-unimpaired""""
nnoremap <silent> [a :previous<CR>
nnoremap <silent> ]a :next<CR>
nnoremap <silent> [A :first<CR>
nnoremap <silent> ]A :last<CR>
nnoremap <silent> [b :bprevious<CR>
nnoremap <silent> ]b :bnext<CR>
nnoremap <silent> [B :bfirst<CR>
nnoremap <silent> ]B :blast<CR>
nnoremap <silent> [l :lprevious<CR>
nnoremap <silent> ]l :lnext<CR>
nnoremap <silent> [L :lfirst<CR>
nnoremap <silent> ]L :llast<CR>
nnoremap <silent> [<C-L> :lpfile<CR>
nnoremap <silent> ]<C-L> :lnfile<CR>
nnoremap <silent> [q :cprevious<CR>
nnoremap <silent> ]q :cnext<CR>
nnoremap <silent> [Q :cfirst<CR>
nnoremap <silent> ]Q :clast<CR>
nnoremap <silent> [<C-Q> :cpfile<CR>
nnoremap <silent> ]<C-Q> :cnfile<CR>
nnoremap <silent> [t :tprevious<CR>
nnoremap <silent> ]t :tnext<CR>
nnoremap <silent> [T :tfirst<CR>
nnoremap <silent> ]T :tlast<CR>
nnoremap <silent> [<C-T> :ptprevious<CR>
nnoremap <silent> ]<C-T> :ptnext<CR>
nnoremap <silent> [<space> :<C-u>put!=repeat([''],v:count)<bar>']+1<cr>
nnoremap <silent> ]<space> :<C-u>put =repeat([''],v:count)<bar>'[-1<cr>

" there is strlen(), strchars() and strwidth(), but only strdisplaywidth is
" capable of dealing with tabs
" show full path to the file as long as strchars(path) < 100
" set statusline=%-.100F\
"\ %m\

" function! ChangeStatuslineColor()
"   " if (mode() ==# 'i')
"     " exe 'hi! StatusLine ctermfg=yellow ctermbg=black'
"   if (mode() ==# '!')
"     exe 'hi! StatusLine ctermfg=cyan ctermbg=black'
"   else
"     exe 'hi! StatusLine ctermfg=white ctermbg=black'
"   endif
"   return ''
" endfunction
" set statusline+=%{ChangeStatuslineColor()}

" highlight statusline ctermfg=yellow ctermbg=black
"
" hi InsertColor ctermbg=yellow ctermfg=black

" Syntax {{{1

augroup cyka
au BufRead,BufNewFile *.nix set filetype=nix
augroup end

" Miscellaneous {{{1

" vi:fdm=marker
