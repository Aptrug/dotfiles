" let b:ale_linters = ['ccls',
" 			\ 'cc',
"             \ 'clangd',
" 			\ 'clangtidy',
" 			\ 'cpplint',
" 			\ 'cppcheck']
"
" let b:ale_fixers = ['clang-format', 'clangtidy']
"
" let b:ale_c_clangformat_style_option = '{
" 			\ UseTab: Always,
" 			\ TabWidth: 4,
" 			\ IndentWidth: 4,
" 			\ ColumnLimit: 80
" 			\ }'
" let b:ale_c_cc_options = '-Weverything'
" let b:ale_cpp_clang_options = '-Weverything'
" let b:ale_c_cppcheck_options = '--enable=all'
" " cpplint is c++ only
" " let b:ale_cpp_cpplint_options = '--filter=-legal/copyright'


setlocal makeprg=/home/pactrag/.config/nvim/ftplugin/lint.sh\ c\ %
setlocal equalprg=clang-format\
			\ --style='{UseTab:\ Always,\
			\ TabWidth:\ 4,\
			\ IndentWidth:\ 4,\
			\ ColumnLimit:\ 80}'\ %

function! AutoFormat()
        let l:winview = winsaveview()
        silent normal! gg=G
        call winrestview(l:winview)
endfunction

augroup shsyntaxcheck
  " autocmd BufWrite <buffer> call AutoFormat()
augroup END

augroup LintOnSave
  autocmd! BufWritePost,BufEnter <buffer> silent make
  " autocmd BufWritePost * silent make! <afile> | silent redraw!
augroup END

augroup show_cwindow_on_save
    autocmd QuickFixCmdPost [^l]* cwindow
augroup END
