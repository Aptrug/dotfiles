" let b:ale_linters = [ 'nix-instantiate', 'nix', 'rnix_lsp' ]
" let b:ale_fixers = [ 'nixpkgs-fmt' ]

setlocal makeprg=nix-linter\ %
setlocal equalprg=nixpkgs-fmt\ <\ /dev/stdin
function! AutoFormat()
        let l:winview = winsaveview()
        silent normal! gg=G
        call winrestview(l:winview)
endfunction

augroup shsyntaxcheck
  " autocmd BufWrite <buffer> call AutoFormat()
augroup END

augroup LintOnSave
  autocmd! BufWritePost,BufEnter <buffer> silent make
  " autocmd BufWritePost * silent make! <afile> | silent redraw!
augroup END

augroup show_cwindow_on_save
    autocmd QuickFixCmdPost [^l]* cwindow
augroup END

