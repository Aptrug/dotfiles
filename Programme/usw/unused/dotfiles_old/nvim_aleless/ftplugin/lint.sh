#!/bin/sh

case "$1" in
c)
	clang -o/dev/null -Weverything -fno-caret-diagnostics "$2"
	clang-tidy --checks='*' "$2" 2>/dev/null | grep '^\/'
	exec cppcheck --quiet \
		--enable=warning,style,performance,portability,unusedFunction \
		--template=vs "$2"
	;;
lua)
	luac -p "$2"
	luacheck --max-line-length 80 --max-code-line-length 80 \
		--max-string-line-length 80 --max-comment-line-length 80 \
		--formatter plain "$2"
	;;
python)
	tac /home/pactrag/.cache/clipboard_history.txt | fzf --no-sort --reverse |
		xsel -bi
	;;
*)
	printf 'illegal option\n'
	;;
esac
