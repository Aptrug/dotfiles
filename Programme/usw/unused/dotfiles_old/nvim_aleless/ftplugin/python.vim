setlocal makeprg=flake8\
			\ --max-line-length\ 80\
			\ --format='\\%(path)s\\\|\\%(row)d\
			\ col\ \\%(col)d\\\|\
			\ \\%(text)s\ [\\%(code)s]'\ %
setlocal errorformat=%o
setlocal equalprg=black\ --line-length\ 80\ --quiet\ -

function! AutoFormat()
        let l:winview = winsaveview()
        silent normal! gg=G                         
        call winrestview(l:winview)
endfunction
" 
augroup shsyntaxcheck
  "autocmd BufWrite <buffer> call AutoFormat()         
augroup END                    
" 
augroup LintOnSave
  autocmd! BufWritePost,BufEnter <buffer> silent make
augroup END
" 
augroup show_cwindow_on_save
    autocmd QuickFixCmdPost [^l]* cwindow
augroup END
