setlocal makeprg=/home/pactrag/.config/nvim/ftplugin/lint.sh\ lua\ %
" TODO figure out why errorformat is needed
" setlocal errorformat=%o
setlocal errorformat=luac:\ %f:%l:\ %m
" setlocal makeprg=luac\ %\ -p\ -
setlocal equalprg=lua-format\ --use-tab\ %\ 2>/dev/null
function! AutoFormat()
        let l:winview = winsaveview()
        silent normal! gg=G
        call winrestview(l:winview)
endfunction

augroup shsyntaxcheck
  autocmd BufWrite <buffer> call AutoFormat()
augroup END

augroup LintOnSave
  autocmd! BufWritePost,BufEnter <buffer> silent make
  " autocmd BufWritePost * silent make! <afile> | silent redraw!
augroup END

augroup show_cwindow_on_save
    autocmd QuickFixCmdPost [^l]* cwindow
augroup END
