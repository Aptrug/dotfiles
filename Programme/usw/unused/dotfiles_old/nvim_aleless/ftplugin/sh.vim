" let b:ale_linters = ['shellcheck', 'shell', 'language_server']
" let b:ale_fixers = ['shfmt']
" let b:ale_sh_shellcheck_options = '--enable=all'
" " it is now safe to enable the next option
" " let b:ale_fix_on_save = 0
" " let b:ale_sh_bashate_options = '--max-line-length 80'
" " -i 0 is for tabs
" let b:ale_sh_shfmt_options = '-i 0'
" 
" let b:ale_sh_language_server_executable =
" 			\ '/home/pactrag/.local/share/node/bin/bash-language-server'

setlocal equalprg=awk\ --file\ -\ --pretty-print=-
setlocal makeprg=shellcheck\ --format=gcc\ --enable=all\ %
" setlocal formatprg=shellharden\ --transform

function! AutoFormat()
        " setlocal equalprg=shellharden\ --transform\ /dev/stdin
        setlocal equalprg=shfmt\ -i\ 0
        let l:winview = winsaveview()
        silent normal! gg=G
        call winrestview(l:winview)
        " silent normal! zz
        setlocal equalprg=awk\ --file\ -\ --pretty-print=-
endfunction

augroup shsyntaxcheck
  autocmd BufWrite <buffer> call AutoFormat()
augroup END

augroup LintOnSave
  autocmd! BufWritePost,BufEnter <buffer> silent make
  " autocmd BufWritePost * silent make! <afile> | silent redraw!
augroup END

augroup show_cwindow_on_save
    autocmd QuickFixCmdPost [^l]* cwindow
augroup END
