#include <stdio.h>
#include <time.h>
#include <unistd.h>

int main() {
  time_t timer;
  char buffer[26];
  for (;;) {
    struct tm *tm_info;

    timer = time(NULL);
    tm_info = localtime(&timer);

    strftime(buffer, 32, "%H:%M:%S, %a, %d %b", tm_info);
    /*printf('%s', strftime);*/
    /*puts(buffer);*/
    printf("%s\r", buffer);
    fflush(stdout);
    sleep(1);
  };
  return 0;
}
