#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int d = 0;
int readf(const char *x) {
  int r = 0;
  FILE *fp = fopen(x, "r");
  fscanf(fp, "%d", &r);
  fclose(fp);
  return r;
}
int main() {
  char td[128];
  sprintf(td, "/sys/class/net/wlp3s0/statistics/rx_bytes");
  int _d = 0;
  while (1) {
    d = readf(td);
    if (_d > 0) {
      printf("%.2f KiB/s\r", (float)(d - _d) / 1024);
      fflush(stdout);
    }
    _d = d;
    sleep(1);
  }
  return 0;
}
