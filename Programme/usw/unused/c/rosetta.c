#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char str[100];
const char d[2] = " ";
char *token;
int i = 0, times;
long int sum = 0, idle, lastSum = 0, lastIdle = 0;
long double idleFraction;

int main(void) {
  for (;;) {
    FILE *fp = fopen("/proc/stat", "r");
    i = 0;
    fgets(str, 100, fp);
    fclose(fp);
    token = strtok(str, d);
    sum = 0;
    while (token != NULL) {
      token = strtok(NULL, d);
      if (token != NULL) {
        sum += atoi(token);

        if (i == 3)
          idle = atoi(token);

        i++;
      }
    }
    idleFraction = 100 - (idle - lastIdle) * 100.0 / (sum - lastSum);
    printf("%d %%\r", (int)(idleFraction));
    fflush(stdout);

    lastIdle = idle;
    lastSum = sum;

    sleep(1);
  }
  return 0;
}
