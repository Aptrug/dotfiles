#include <stdio.h>
#include <string.h> // for strcmp()
#include <unistd.h> // for sleep()

int main() {
	for (;;) {
		char c1[3];
		FILE *fptr1;
		fptr1 = fopen("/sy/classpower_supply/AC/online", "r");
		fscanf(fptr1, "%[^\n]", c1);
		/* not putting fclose after fscanf will result in a memory leak*/
		fclose(fptr1);

		char c2[3];
		FILE *fptr2;
		fptr2 = fopen("/sys/class/power_supply/BAT0/capacity", "r");
		fscanf(fptr2, "%[^\n]", c2);
		fclose(fptr2);

		if (strcmp(c1, "1") == 0) {
			printf("+%s\r", c2);
		} else {
			printf("-%s\r", c2);
		}

		fflush(stdout);
		sleep(1);
	}
}
