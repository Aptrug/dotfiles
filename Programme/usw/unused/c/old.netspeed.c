#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

long readf(const char *x) {
  long r = 0;
  FILE *fp = fopen(x, "r");
  fscanf(fp, "%ld", &r);
  fclose(fp);
  return r;
}
int main() {
  char td[64], tp[64];
  sprintf(td, "/sys/class/net/wlp3s0/statistics/rx_bytes");
  sprintf(tp, "/sys/class/net/wlp3s0/statistics/tx_bytes");
  long _d = 0;
  long _D = 0;

  char rx_packets[64], tx_packets[64];
  while (1) {
    FILE *down, *up;
    int rx, tx;
    long d = 0;
    d = readf(td);
    if (_d > 0) {
      printf("NETSPEED: %.2f KiB/s, ", (double)(d - _d) / 1024);
    }
    _d = d;
    down = fopen("/sys/class/net/wlp3s0/statistics/rx_packets", "r");
    up = fopen("/sys/class/net/wlp3s0/statistics/tx_packets", "r");
    fgets(rx_packets, 64, down);
    fclose(down);
    fgets(tx_packets, 64, up);
    fclose(up);
    rx = atoi(rx_packets);
    tx = atoi(tx_packets);
    int converted_rx = (int)(rx / 1024);
    int converted_tx = (int)(tx / 1024);
    /*TNU:total net usage since last boot*/
    printf("TNU: %d\r", converted_rx + converted_tx);
    fflush(stdout);
    sleep(1);
  }
}
