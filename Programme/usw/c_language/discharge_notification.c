/* 
* The notification code is taken from:
* https://wiki.archlinux.org/title/Desktop_notifications#C
* Compile it with:
* clang -O3 $(pkg-config --cflags --libs libnotify) -lnotify discharge_notification.c -o discharge_notification.eil
*/

#include <libnotify/notify.h>
#include <stdio.h>
/* For putenv */
#include <stdlib.h>

int main() {
	FILE *file;
	char battery[16];
	file = fopen("/home/pactrag/Programme/sysinfo/misc/battery", "r");
	fgets(battery, 16, file);
	fclose(file);

	/* Get the first character from string, which is either '+' or '-'. */
	char battery_status;
	battery_status = battery[0];

	/* It seems like single quotes must be used with chars. */
	if (battery_status == '-') {
		putenv("DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus");
		notify_init("Battery draining");
		NotifyNotification *Alarm = notify_notification_new(
			"DISCHARGING", battery,
			0);
		notify_notification_set_timeout(Alarm, 5000);
		notify_notification_show(Alarm, NULL);
		g_object_unref(G_OBJECT(Alarm));
		notify_uninit();
		return 0;
	}
}
