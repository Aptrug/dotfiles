#include <ctype.h>
#include <stdio.h>
#include <string.h>

int main() {
  FILE *file = fopen("/proc/meminfo", "r");
  char line[256];
  char *e;

  while (fgets(line, sizeof(line), file)) {
    if (strstr(line, "MemFree") != NULL) {
      e = strchr(line, ':');
      e++;
      while (isspace((unsigned char)*e))
        e++;
      printf("%s", e);
    }
  }
  fclose(file);
}
