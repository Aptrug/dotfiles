#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main() {

	while (1) {
		/* Variables */
		FILE *file;
		int i;

		/* Netspeed */
		char netspeed[16];
		file = fopen("/home/pactrag/Programme/sysinfo/misc/netspeed", "r");
		i = 0;
		while (fgets(netspeed, sizeof(netspeed), file)) {
			i++;
			if (i == 2) {
				break;
			}
		}
		fclose(file);
		/* Remove the trailing line */
		netspeed[strlen(netspeed) - 1] = 0;

		/* Memory */
		char memory[16];
		file = fopen("/home/pactrag/Programme/sysinfo/misc/ramUsed", "r");
		fgets(memory, 16, file);
		fclose(file);
		memory[strlen(memory) - 1] = 0;

		/* CPU */
		char cpu[16];
		file = fopen("/home/pactrag/Programme/sysinfo/misc/cpu", "r");
		fgets(cpu, 16, file);
		fclose(file);

		/* Battery */
		char battery[16];
		file = fopen("/home/pactrag/Programme/sysinfo/misc/battery", "r");
		fgets(battery, 16, file);
		fclose(file);

		/* Bring it together */
		printf("NS: %sk | MEM: %s | CPU: %s | BAT: %s\r", netspeed, memory, cpu,
			   battery);

		/* fflush must used if printf has \r */
		fflush(stdout);
		sleep(1);
	}
}
