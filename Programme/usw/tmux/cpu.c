#include <stdio.h>
#include <unistd.h>
int main(void) {

  long double a[4], b[4], loadavg;
  FILE *fp;
  fp = fopen("/proc/stat", "r");
  fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &a[0], &a[1], &a[2], &a[3]);
  fclose(fp);

  while (1) {
    /*long double loadavg;*/
    fp = fopen("/proc/stat", "r");
    fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &b[0], &b[1], &b[2], &b[3]);
    fclose(fp);

    loadavg = ((b[0] + b[1] + b[2]) - (a[0] + a[1] + a[2])) /
              ((b[0] + b[1] + b[2] + b[3]) - (a[0] + a[1] + a[2] + a[3]));
    int rounded = (int)(loadavg * 100);
    printf("%d %%\r", rounded);
    fflush(stdout);

    fp = fopen("/proc/stat", "r");
    fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &a[0], &a[1], &a[2], &a[3]);
    fclose(fp);

    fflush(stdout);
    sleep(1);
  }
}
