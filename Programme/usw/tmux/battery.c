#include <stdio.h>
#include <string.h> // for strcmp()
#include <unistd.h> // for sleep()

int main() {
  for (;;) {
    char percentage[3];
    FILE *capacity;
    capacity = fopen("/sys/class/power_supply/BAT0/capacity", "r");
    fgets(percentage, 3, capacity);
    fclose(capacity);

    char power[3];
    FILE *online;
    online = fopen("/sys/class/power_supply/AC/online", "r");
    fgets(power, 3, online);
    char *key = strtok(power, "\n");
    fclose(online);

    if (strcmp(key, "1") == 0) {
      printf("\r");
    } else {
      printf("-%s %% | \r", percentage);
    }

    fflush(stdout);
    sleep(1);
  }
}
