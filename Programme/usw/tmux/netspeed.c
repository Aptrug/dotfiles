#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for sleep()

int main() {

  long _d = 0;
  for (;;) {
    char down[64];
    FILE *capacity;
    capacity = fopen("/sys/class/net/wlp3s0/statistics/rx_bytes", "r");
    fgets(down, 64, capacity);
    fclose(capacity);

    char up[64];
    FILE *online;
    online = fopen("/sys/class/net/wlp3s0/statistics/tx_bytes", "r");
    fgets(up, 64, online);
    fclose(online);

    int rx = atoi(down);
    int tx = atoi(up);
    long d = (int)(rx + tx);
    printf("NS: %.2f KiB/s, ", (double)(d - _d) / 1024);
    printf("TNU: %ld MiB\r", d / 1048576);
    fflush(stdout);
    _d = d;
    sleep(1);
  }
}
