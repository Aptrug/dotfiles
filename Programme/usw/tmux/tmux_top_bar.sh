#!/bin/sh

set -efu

while true; do
    #for="$(uptime | awk '{print $3}')"
    #since="$(who -b | awk '{print $4}')"

    #printf 'running for %s since %s\r' "$for" "$since" >/home/pactrag/Programme/sysinfo/misc/forSince

    uptime="$(cat /home/pactrag/Programme/sysinfo/misc/uptime)"
    printf '%s\r' "$uptime"
    sleep 60
done
