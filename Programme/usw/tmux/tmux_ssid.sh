#!/bin/sh

set -efu

while true; do
    ssid="$(iwgetid --raw)"
    printf '%s\r' "$ssid"
    sleep 60
done
