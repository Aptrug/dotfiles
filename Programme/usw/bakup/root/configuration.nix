{ pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];


  boot = {
    supportedFilesystems = [ "ntfs" ]; # for Windows 10
    cleanTmpDir = true; # tmp is cleaned on boot
    loader = {
      efi.canTouchEfiVariables = true;
      timeout = 3;
    };
    loader.systemd-boot = {
      enable = true;
      # I do not need all these configurations
      configurationLimit = 1;
    };
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
    #supportedLocales = [ "de_DE.UTF-8" ];
  };


  time.timeZone = "Europe/Reykjavik";

  fileSystems = {
    "/mnt/windows10" =
      {
        device = "/dev/disk/by-uuid/9E8C4B068C4AD903";
      };
  };

  environment =
    {
      defaultPackages = [ ];
      systemPackages =
        let
          custom_packages = with pkgs; {

            luaformatter = stdenv.mkDerivation rec {
              pname = "luaformatter";
              version = "1.3.5";

              src = fetchFromGitHub {
                owner = "koihik";
                repo = "luaformatter";
                rev = version;
                sha256 = "163190g37r6npg5k5mhdwckdhv9nwy2gnfp5jjk8p0s6cyvydqjw";
                fetchSubmodules = true;
              };

              nativeBuildInputs = [ cmake ];

              installPhase = ''
                runHook preInstall
                mkdir -p $out/bin
                cp lua-format $out/bin
                runHook postInstall
              '';
            };

          }; in
        with pkgs // custom_packages; [
          # Download utility that supports HTTP(S), FTP, BitTorrent
          aria2

          # Code formatter for Lua
          luaformatter

          # File type identification utility
          file

          # Nix code formatter for nixpkgs
          nixpkgs-fmt

          # An image viewing/manipulation program
          imagemagick

          # Fork of Vim aiming to improve user experience, plugins, and GUIs
          neovim

          # the fast distributed version control system
          git

          # A terminal multiplexer
          tmux

          # Shell script analysis tool
          shellcheck

          # Lint tool for Vim script Language
          vim-vint

          # Clang utilities suite (provides clang-tidy and clang-format
          # along others)
          clang-tools

          # Complete solution to record, convert and stream audio and video
          ffmpeg-full

          # Format shell programs
          shfmt

          # Command-line fuzzy finder
          fzf

          # Tool to help find memory-management problems in programs
          valgrind

          # Command-line X11 automation tool
          xdotool

          # TeX Live core distribution
          texlive.combined.scheme-minimal

          # POSIX compliant shell that aims to be as small as possible
          dash

          # XSel is a command-line program for getting and setting the contents
          # of the X selection
          xsel

          # Lightweight brightness control tool
          brightnessctl

          # lightweight programming language designed for extending applications
          lua

          # Simple tool for viewing MTP devices as FUSE filesystems
          go-mtpfs

          # Standalone web browser from mozilla.org
          firefox

          # A web browser built for speed, simplicity, and security
          chromium

          # Utilities for rescue and embedded systems
          # busybox

          # Adjusts the color temperature of your screen according to your
          # surroundings.
          redshift

          # Command-line file archiver with high compression ratio
          p7zip

          # Customizable and lightweight notification-daemon
          dunst

          # A diagnostic, debugging and instructional userspace tracer
          strace

          # FUSE client based on the SSH File Transfer Protocol
          sshfs

          # C language family frontend for LLVM
          clang

          # A tool for static C/C++ code analysis
          cppcheck

          # Uncompromising Python code formatter
          python3Packages.flake8

          # The modular source code checker: pep8, pyflakes and co
          python3Packages.black
          python3Packages.yapf

          # A tool for linting and static analysis of Lua code
          luaPackages.luacheck

          # nix linter
          nixpkgs-lint

          # nix linter
          nix-linter

          # (
          #   perl.withPackages (
          #     ps: with ps; [
          #       # Parses and beautifies perl source
          #       PerlTidy
          #       # Critique Perl source code for best-practices
          #       PerlCritic
          #     ]
          #   )
          # )
        ];
    };

  fonts = {
    fontconfig = {
      defaultFonts = {
        monospace = [ "Hack" ];
        sansSerif = [ "Noto Sans" ];
        serif = [ "Noto Serif" ];
      };
    };
    fonts = with pkgs; [
      #eb-garamond
      noto-fonts
      # noto-fonts-emoji
      hack-font
    ];
  };


  # Enable sound.
  sound.enable = true;
  hardware = {
    pulseaudio = {
      enable = true;
      configFile = pkgs.runCommand "default.pa" { } ''
        ${pkgs.gnused}/bin/sed '
        /^.ifexists module-esound-protocol-unix.so$/,+2d
        ' ${pkgs.pulseaudio}/etc/pulse/default.pa > $out
      '';

    };
  };

  users.users.pactrag = {
    isNormalUser = true;
    # shell = pkgs.fish;
    extraGroups = [
      "adbusers"
    ];
    openssh.authorizedKeys.keys =
      let
        # for the sake of sticking to the 80 column length rule
        SSHkey = "ssh-ed25519 "
          + "AAAAC3NzaC1lZDI1NTE5AAAAIBDw/"
          + "eQe3RCu180FyfHSPnX6oXjdUf8UBGNPtsrUh2u8 fajita";
      in
      [
        SSHkey
      ];
  };

  programs = {
    adb.enable = true;
  };

  services = {

    # I tried hard, but aria2.service I wrote did not work
    aria2 = {
      enable = true;
      extraArguments = "
        --conf-path= /home/pactrag/.config/aria2/aria2.conf
      ";
    };

    xserver = {
      enable = true;
      layout = "us, ar";
      # consult manpages.debian.org/testing/xkb-data/xkeyboard-config.7.en.html
      xkbOptions = "compose:caps, shift:both_capslock, grp:win_space_toggle";
      # xkbOptions = "compose:caps, shift:both_capslock";
      # enable touchpad support
      libinput.enable = true;

      windowManager.bspwm = {
        enable = true;
      };
      displayManager = {
        defaultSession = "none+bspwm";
        autoLogin.user = "pactrag";
      };

      # trackpad speed
      inputClassSections = [
        ''
          Identifier "TrackPoint"
          MatchProduct "TPPS/2 IBM TrackPoint"
          Option "Accel Speed" "-0.4"
        ''
      ];

    };

    logind.lidSwitch = "lock";

    openssh = {
      enable = true;
      # make the next cipher the only allowed one (the fastest of them)
      ciphers = [ "aes128-ctr" ];
    };

    tlp = {
      enable = true;
      settings = {
        START_CHARGE_THRESH_BAT0 = 75;
        STOP_CHARGE_THRESH_BAT0 = 80;
      };
    };

    # for clipboard support
    # greenclip.enable = true;

  };

  networking = {
    # Define your hostname.
    hostName = "nixos";
    firewall.enable = false;
    useDHCP = false;
    wireless = {
      # Enables wireless support via wpa_supplicant.
      enable = true;
      networks = {
        Orange-F666 = {
          # psk = "=8RNN9rk";
          # psk = "Schlagmann47";
          psk = "S@dik2001";
          # psk = "0JF0A78MQ5F";
        };
        # Orange-9E7A = {
        #   psk = "33550336";
        # };
      };
    };
    supplicant.wlp3s0 = {
      userControlled.enable = true; # this install wpa_cli
      configFile.path = "/etc/wpa_supplicant.conf";
      configFile.writable = true;
    };

    #enableIPv6 = false;
    interfaces.wlp3s0.ipv4.addresses = [{
      address = "192.168.1.174";
      prefixLength = 24;
    }];
    defaultGateway = "192.168.1.1";
    # use cloudflare dns
    nameservers = [ "1.1.1.1" "1.0.0.1" ];
  };

  security = {
    # disable sudo
    sudo.enable = false;
  };

  # release version of the first install of the system, leave it alone
  system.stateVersion = "20.09";
}
# it works
