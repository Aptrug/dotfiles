#!/bin/sh

set -efu

fzf --nth="2" --exact --preview '
curl --silent dict://dict.org/d:{-1}:fd-deu-eng |
	grep --invert-match "^[0-9]"'\
	</home/pactrag/Programme/usw/Artikel/deutscheArtikel.txt
