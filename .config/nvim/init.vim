" Main {{{1

" Convert bytes to convenient units, used in the statusline
function! FileSize(bytes)
	let l:bytes = a:bytes
	let l:sizes = ['B', 'K', 'M', 'G']
	let l:i = 0
	while l:bytes >= 1024
		let l:bytes = l:bytes / 1024.0
		let l:i += 1
	endwhile
	return l:bytes > 0 ? printf('%.1f%s', l:bytes, l:sizes[l:i]) : ''
endfunction

function! QuickfixLines()
	return printf("%d", len(getqflist()))
	" return len(getqflist())
endfunction

let g:warning_count = 2
set clipboard+=unnamedplus
			\ textwidth=80
			\ colorcolumn=+0
			\ number
			\ undofile
			"\ this is a comment
			\ ignorecase
			\ smartcase
			"\ language-specific completion with C-x C-o
			\ omnifunc=syntaxcomplete#Complete
			\ showmatch
			\ cursorline
			\ noexpandtab
			\ tabstop=4
			\ softtabstop=4
			\ shiftwidth=4
			\ splitbelow
			\ splitright
			\ nojoinspaces
			\ directory=./
			\ autochdir
			\ inccommand=nosplit
			\ statusline=f:%f\
			\ \|\
			"\ size
			\ s:%{FileSize(line2byte('$')+len(getline('$')))}\
			\ \|\
			"\ t
			\ t:%Y\
			\ \|\
			"\ buffNum
			\ b:%{len(filter(range(1,bufnr('$')),'buflisted(v:val)'))}\
			\ %#warningmsg#%r%*\
			\ %=\
			\ %#warningmsg#%{&modified?'Modified':''}%*\
			"\ Last modification date. I do not need it.
			"\ m:%{strftime('%Y%m%dT%H%M%S',getftime(expand('%:p')))}\
			\ \|\
			"\ problems (ALE)
			"\ Q:%{ale#statusline#Count(bufnr(''))['total']}\
			\ Q:%{QuickfixLines()}\
			\ \|\
			"\ percentage
			\ p:%p%%\
			\ \|\
			"\ column
			\ c:%v/%{strdisplaywidth(getline('.'))}\
			\ \|\
			\ line:%l/%L


function! s:DiffGitWithSaved()
        let filename = expand('%')
        let diffname = tempname()
        execute 'silent w! '.diffname
        execute '!git diff --no-index -- '.shellescape(filename).' '.diffname
endfunction
com! DiffGitSaved call s:DiffGitWithSaved()

" disable annoying providers causing false alarms
let g:loaded_python_provider = 0
let g:loaded_ruby_provider = 0
let g:loaded_node_provider = 0

" Terminal {{{1
let g:termWinVar = 0 " a variable needed by Term_toggle()
function! Term_toggle(height)
	if win_gotoid(g:termWinVar)
		hide
	else
		" vert new
		new
		" no numbering for terminal buffer
		setlocal nonumber
		try
			exec 'buffer ' . g:term_buf
		catch
			call termopen($SHELL, {'detach': 0})
			let g:term_buf = bufnr('')
		endtry
		let g:termWinVar = win_getid()
	endif
endfunction
" <C-q> is unassigned in both Neovim and Emacs
" silent prevents 'Term_toggle(10)' from showing in the command line
nnoremap <silent> <F4> :call Term_toggle(10)<cr>
tnoremap <silent> <F4> <C-\><C-n>:call Term_toggle(10)<cr>

" Netrw {{{1
" Tree view
let g:netrw_liststyle=3

function! ToggleNetrw()
	let i = bufnr('$')
	let wasOpen = 0
	while (i >= 1)
		if (getbufvar(i, '&filetype') ==# 'netrw')
			silent exe 'bwipeout ' . i
			let wasOpen = 1
		endif
		let i-=1
	endwhile
	if !wasOpen
		silent Lexplore!
	endif
endfunction

" toggle netrw
noremap <silent> <F3> :call ToggleNetrw() <CR>

" Spelling {{{1

" Set the spelling language.
set spelllang=en_us

" Enable spell checking (disabled by default).
" set spell

" Quickfix/Location list toggle {{{1

" needed by the other next function
function! GetBufferList()
	redir =>buflist
	silent! ls!
	redir END
	return buflist
endfunction

function! ToggleList(bufname, pfx)
	let buflist = GetBufferList()
	for bufnum in map(filter(split(buflist, '\n'), 'v:val =~ "'.a:bufname.'"'),
				\'str2nr(matchstr(v:val, "\\d\\+"))')
		if bufwinnr(bufnum) != -1
			exec(a:pfx.'close')
			return
		endif
	endfor
	if a:pfx ==# 'l' && len(getloclist(0)) == 0
		echohl ErrorMsg
		echo 'Location List is Empty.'
		return
	endif
	let winnr = winnr()
	exec(a:pfx.'open')
	if winnr() != winnr
		wincmd p
	endif
endfunction

nnoremap <silent> <F1> :call ToggleList("Location List", 'l')<CR>
nnoremap <silent> <F2> :call ToggleList("Quickfix List", 'c')<CR>
" Compile {{{1
function! CompileRun()
	write
	"e!
	vsplit
	" terminal /home/pactrag/Programme/usw/shell/inpath/kompile.sh %
	" bufdo e!
	terminal /home/pactrag/Programme/usw/shell/inpath/kompile.sh %
endfunc

" <leader>r to compile and run
noremap <leader>r :call CompileRun()<CR>

" Independant autocmmands {{{1

" check for changes outside the file when the cursor
" has not moved for 4 seconds (by default)
augroup reload_file_on_change
	autocmd CursorHold * checktime
augroup end

" Close quickfix if it is the last window
augroup QFClose
	autocmd WinEnter * if winnr('$') == 1 && &buftype == "quickfix"|q|endif
augroup END

" Less intrusive hlsearch
augroup hlsearchNoMore
	" hide the searched string when entering insert mode
	autocmd InsertEnter * :let b:_search=@/|let @/=''
	" requip the last searched string
	autocmd InsertLeave * :let @/=get(b:,'_search','')
augroup end

" Cursor position
augroup rememberCursorPosition
	autocmd!
	autocmd BufReadPost *
				\ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
				\ | execute "normal! g`\"zvzz"
				\ | endif
augroup END

" disable auto-comment insertion
augroup disable_auto_comment_insertion
	autocmd OptionSet * set formatoptions-=cro
augroup end

" opening help in new tab instead
augroup HelpBuffer
	" move the help split to another tab right after its creation
	autocmd filetype help execute "normal! \<C-w>\T"
augroup END

augroup no_number_terminal
	" TermOpen event is triggerd after opening the terminal window for the first
	" time, BufEnter event is triggered after unhiding the terminal window thus,
	" both are needed
	autocmd BufEnter,TermOpen term://* startinsert
augroup end

" augroup nix_syntax
" 	autocmd BufRead,BufNewFile *.nix set filetype=nix
" augroup END

" augroup save_as_you_type
" 	autocmd TextChanged,TextChangedI todo.txt silent write
" augroup END
" Independant keybindings {{{1

" Emacs
noremap Q <Nop>
noremap <silent> ZS :write<CR>

" hide highlighed search on hitting return
nnoremap <silent> <CR> :nohlsearch<CR><CR>

" Remapping leader to spacebar
let mapleader = "\<Space>"
let maplocalleader = ';'

" disabling spacebar in normal mode
nnoremap <Space> <NOP>
nnoremap <S-Space> <NOP>

" gm to go in the half of current line (replaces defaults)
map gm :call cursor(0, virtcol('$')/2)<CR>

" keyboard shortcuts to certain files
nnoremap gev :tabnew $MYVIMRC<CR>
nnoremap gsv :source $MYVIMRC<CR>
" nnoremap gfv :tabnew /home/pactrag/.config/nvim/configs/functions.vim<CR>
" nnoremap gmv :tabnew /home/pactrag/.config/nvim/configs/main.vim<CR>
" nnoremap gpv :tabnew /home/pactrag/.config/nvim/configs/plugins.vim<CR>
" nnoremap gkv :tabnew /home/pactrag/.config/nvim/configs/keybindings.vim<CR>

" quickly search for a flag in a man page
nnoremap <C-_> /^\s\+

"keep visual mode after indent
vnoremap > >gv
vnoremap < <gv
"vnoremap y ygv

" make c cut
nnoremap cc "_dd
vnoremap c "_d
vnoremap c "_d
nnoremap c "_d
nnoremap C "_D
" make remove actually remove
nnoremap x "_x
nnoremap X "_X
vnoremap x "_x
vnoremap X "_X
nnoremap s "_s
nnoremap S "_S
vnoremap s "_s
vnoremap S "_S
"nnoremap d "_d
"vnoremap d "_d
"nnoremap D "_D
"vnoremap D "_D
"nnoremap dd "_dd

" make paste in visual mode not overwrite the last yank
"vnoremap p "0p

" remap up arrow and down arrow to ctrl-e ctrl-y
nnoremap <Down> <C-e>
nnoremap <Up> <C-y>

" move up/down regardless of line length
nnoremap <C-j> gj
nnoremap <C-k> gk

"""vim-unimpaired""""
nnoremap <silent> [a :previous<CR>
nnoremap <silent> ]a :next<CR>
nnoremap <silent> [A :first<CR>
nnoremap <silent> ]A :last<CR>
nnoremap <silent> [b :bprevious<CR>
nnoremap <silent> ]b :bnext<CR>
nnoremap <silent> [B :bfirst<CR>
nnoremap <silent> ]B :blast<CR>
nnoremap <silent> [l :lprevious<CR>
nnoremap <silent> ]l :lnext<CR>
nnoremap <silent> [L :lfirst<CR>
nnoremap <silent> ]L :llast<CR>
nnoremap <silent> [<C-L> :lpfile<CR>
nnoremap <silent> ]<C-L> :lnfile<CR>
nnoremap <silent> [q :cprevious<CR>
nnoremap <silent> ]q :cnext<CR>
nnoremap <silent> [Q :cfirst<CR>
nnoremap <silent> ]Q :clast<CR>
nnoremap <silent> [<C-Q> :cpfile<CR>
nnoremap <silent> ]<C-Q> :cnfile<CR>
nnoremap <silent> [t :tprevious<CR>
nnoremap <silent> ]t :tnext<CR>
nnoremap <silent> [T :tfirst<CR>
nnoremap <silent> ]T :tlast<CR>
nnoremap <silent> [<C-T> :ptprevious<CR>
nnoremap <silent> ]<C-T> :ptnext<CR>
nnoremap <silent> [<space> :<C-u>put!=repeat([''],v:count)<bar>']+1<cr>
nnoremap <silent> ]<space> :<C-u>put =repeat([''],v:count)<bar>'[-1<cr>

" Syntax {{{1

" Make cursorline less intrusive.
highlight CursorLine term=bold cterm=bold

" Status bar color.
" hi StatusLine ctermbg=Black ctermfg=Yellow
" 220 is Gold ('#ffd700' != '#FFCC00')
hi StatusLine ctermbg=Black ctermfg=220
" Miscellaneous {{{1
runtime machen.vim

" stolen from https://gist.github.com/BoltsJ/5942ecac7f0b0e9811749ef6e19d2176
" based on search: \"quickfix\" \"sign place\" in google
" Do not forget to take a look in Github too:
" https://github.com/search?q=extension%3Avim+%22quickfix%22+%22sign+place%22
runtime quickfix_sign.vim

" vi:fdm=marker
