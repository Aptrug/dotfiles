set noexpandtab
			\ tabstop=4
			\ softtabstop=4
			\ shiftwidth=4

let b:fixer='yapf --style="{use_tabs: True}"'

setlocal makeprg=flake8\
			\ --max-line-length\ 80\
			"\ Ignore warnings about tabs
			\ --extend-ignore=W191\
			\ %
setlocal errorformat=%f:%l:%c:\ %m
