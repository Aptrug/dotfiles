" {{{
" let b:ale_linters = ['shellcheck', 'shell', 'language_server']
" let b:ale_fixers = ['shfmt']
" let b:ale_sh_shellcheck_options = '--enable=all'
" let b:ale_sh_shfmt_options = '-i 0'
" }}}

setlocal equalprg=/usr/bin/gawk\ --file\ -\ --pretty-print=-
setlocal errorformat=%f:%l:%c:\ %o:\ %m
setlocal makeprg=shellcheck\
			\ --external-sources\
			\ --format=gcc\
			\ --enable=all\ %
let b:fixer = 'shfmt -i 0'
