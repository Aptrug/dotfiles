" {{{
" let b:ale_linters = ['ccls',
" 			\ 'cc',
"             \ 'clangd',
" 			\ 'clangtidy',
" 			\ 'cpplint',
" 			\ 'cppcheck']
"
" " NOTE enable only one fixer. clangtidy did some nasty stuff in the past.
" let b:ale_fixers = ['clang-format']
"
" let b:ale_c_clangformat_style_option = '{
" 			\ UseTab: Always,
" 			\ TabWidth: 4,
" 			\ IndentWidth: 4,
" 			\ ColumnLimit: 80
" 			\ }'
" let b:ale_c_cc_options = '-Weverything'
" let b:ale_c_cppcheck_options = '--enable=all'
" let b:ale_c_clangtidy_checks = [ '*' ]
"}}}

" Shit starts happening when I remove this one. Trust me.
" setlocal errorformat=%o

setlocal makeprg=/home/pactrag/.config/nvim/after/ftplugin/lint.sh\ c\ %
let b:fixer='clang-format
			\ --style="{
			\ UseTab: Always,
			\ TabWidth: 4,
			\ IndentWidth: 4,
			\ ColumnLimit: 80
			\ }"
			\ /dev/stdin'
