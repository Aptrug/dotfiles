set noexpandtab

" Perltidy does not support tabs. Fighting the system is fruitless.
setlocal expandtab

setlocal errorformat=%f:%l:%c:%m
" --exclude=RequireTidyCode\
setlocal makeprg=/usr/bin/vendor_perl/perlcritic\
	\ --quiet\
	\ --nocolor\
	\ --brutal\
	\ --exclude='Modules::RequireVersionVar'\
	\ --exclude='ControlStructures::ProhibitCascadingIfElse'\
	\ --exclude='RegularExpressions::ProhibitEscapedMetacharacters'\
	\ --exclude='ControlStructures::ProhibitCStyleForLoops'\
	\ --exclude='RegularExpressions::ProhibitEnumeratedClasses'\
	\ --exclude='RegularExpressions::RequireBracesForMultiline'\
	\ --exclude='ControlStructures::ProhibitUnreachableCode'\
	\ --exclude='ControlStructures::ProhibitUnlessBlocks'\
	\ --exclude='ValuesAndExpressions::ProhibitEmptyQuotes'\
	\ --exclude='RegularExpressions::ProhibitUnusualDelimiters'\
	\ --exclude='Variables::ProhibitPunctuationVars'\
	\ --exclude='ValuesAndExpressions::ProhibitNoisyQuotes'\
	\ --exclude='InputOutput::RequireBriefOpen'\
	\ --exclude='Subroutines::ProhibitExcessComplexity'\
	\ --exclude='ValuesAndExpressions::ProhibitImplicitNewlines'\
	\ --exclude='Subroutines::ProhibitExcessComplexity'\
	\ --exclude='CodeLayout::RequireTidyCode'\
	\ --exclude='InputOutput::RequireCheckedSyscalls'\
	\ --exclude='InputOutput::RequireCheckedOpen'\
	\ --exclude='ValuesAndExpressions::RequireInterpolationOfMetachars'\
	\ --exclude='BuiltinFunctions::ProhibitReverseSortBlock'\
	\ --exclude='Subroutines::ProhibitManyArgs'\
	\ --exclude='ValuesAndExpressions::ProhibitMagicNumbers'\
	\ --exclude='RegularExpressions::RequireDotMatchAnything'\
	\ --exclude='RegularExpressions::RequireLineBoundaryMatching'\
	\ --exclude='RegularExpressions::RequireExtendedFormatting'\
	\ --exclude='InputOutput::ProhibitBacktickOperators'\
	\ --exclude='ValuesAndExpressions::RequireUpperCaseHeredocTerminator'\
	\ --exclude='InputOutput::RequireCheckedClose'\
	\ --exclude='ValuesAndExpressions::RequireQuotedHeredocTerminator'\
	\ --verbose\
	\ 1\
	\ %
let b:fixer='/usr/bin/vendor_perl/perltidy --standard-output --quiet'
setlocal equalprg=/usr/bin/vendor_perl/perltidy\ --standard-output\ --quiet
" It means perltidy will work as long as the number of errors in the quickfix
" list does not go over 2
let b:blyat = 2
