" {{{
" let b:ale_linters = ['vint']
" }}}

" It does not work with `/dev/stdin`
setlocal makeprg=vint\ --enable-neovim\ --no-color\ --style-problem\ %
setlocal errorformat=%f:%l:%c:\ %m
let b:fixer = ''
