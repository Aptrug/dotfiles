" {{{
" let b:ale_linters = ['luac', 'luacheck']
" let b:ale_lua_luacheck_options = '--max-line-length 80 \
" 			\ --max-code-line-length 80 \
" 			\ --max-string-line-length 80 \
" 			\ --max-comment-line-length 80 \
" 			\ --formatter plain'
" }}}

setlocal makeprg=/home/pactrag/.config/nvim/after/ftplugin/lint.sh\ lua\ %
" setlocal makeprg=luac\ -p\ %
" setlocal makeprg=luacheck\ --formatter\ plain\ %

" TODO figure this out. do NOT comment it, it will screw up cursor position
setlocal errorformat=%f:%l:%c:\ %m

let b:fixer='lua-format
			\ --column-limit=80
			\ --use-tab
			\ --indent-width=1
			\ --no-keep-simple-control-block-one-line
			\ --no-keep-simple-function-one-line
			\ /dev/stdin'

