#!/bin/sh

PATH=/usr/bin

case "$1" in
c)
	clang -o/dev/null -Weverything -fno-caret-diagnostics "$2"
	# clang-tidy --checks='*' "$2" 2>/dev/null | grep '^\/'
	clang-tidy "$2" 2>/dev/null | grep '^\/'
	# exec cppcheck --quiet \
	# 	--enable=warning,style,performance,portability,unusedFunction \
	# 	--template=vs "$2"
	;;
lua)
	luac -p "$2"
	luacheck --max-line-length 80 --max-code-line-length 80 \
		--max-string-line-length 80 --max-comment-line-length 80 \
		--no-color --codes --formatter plain "$2"
	;;
perl)
	# perl -WTc "$2"
	;;
*)
	printf 'illegal option\n'
	;;
esac
