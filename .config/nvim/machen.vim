" Asynchronous make {{{1
" NOTE: stolen from https://tinyurl.com/yhhczxxc
" }}}
" Make misc {{{1

augroup LintOnSave
	" BufWritePre and BufWrite do not work
	" autocmd! BufWritePost,UIEnter * call Linting_and_Fixing()
	autocmd! BufWritePost * silent! call Linting_and_Fixing()
augroup END


augroup LintOnSomething
	" BufWritePre and BufWrite do not work
	" Do not use BufEnter since the Quickfix list will be unclosable
	autocmd! TabNew,BufWinEnter * call Lint()
augroup END

" Do NOT use this with `Make`, `make` only.
augroup show_cwindow_on_save
	autocmd QuickFixCmdPost [^l]* cwindow
augroup END
" Fixing {{{1

function! Format(variable)
	if &equalprg !=# ''
		" initialize the variable
		let l:previous_equalprg = ''

		" Backup the existing equalprg if it exists.
		let l:previous_equalprg = &equalprg

		let &equalprg = a:variable

		" undojoin makes gg=G undoable
		silent! undojoin | silent normal! 1G=G

		" restore equalprg (evaluates to '' if not defined)
		let &equalprg = l:previous_equalprg
	else
		silent! undojoin | silent normal! 1G=G
	endif
endfunction

function! AutoFormat(var1, var2)
	if ! exists('b:blyat')
		let b:blyat = 1
	endif

	if a:var1 < b:blyat
		call Format(a:var2)
	endif
endfunction


function! Lint()
	if &makeprg !=# 'make'
		silent make
	endif
endfunction


function! Linting_and_Fixing()
	call Lint()
	let l:warnings_number = QuickfixLines()

	let l:winview = winsaveview()
	execute 'silent! undojoin | keeppatterns %s/\s\+$\|\_s*\%$//e'
	call AutoFormat(l:warnings_number, b:fixer)
	call winrestview(l:winview)
endfunction

" vi:fdm=marker
