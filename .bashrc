#!/bin/sh

# declaring TERMUX again prevents SC2154 from being triggered
TERMUX="${TERMUX-}"
alias vi='nvim --clean'
alias bat="cat /sys/class/power_supply/AC/online /sys/class/power_supply/BAT0/capacity"
alias l='ls -alh'
alias termux-clipboard-get='ssh -c aes128-ctr user@${TERMUX-} -p 8022 /data/data/com.termux/files/home/scripts/fajita/getClipboard.sh'
# alias au="ffplay -nodisp -loglevel quiet"
alias inpath="cd /home/pactrag/Programme/usw/shell/inpath"
alias syu='systemctl --user'
alias sud='systemctl --user daemon-reload'
alias jbuu='journalctl --boot --user -u'
alias usw="cd /home/pactrag/Programme/usw"
alias perlcritic_brutal='perlcritic --verbose 11 --brutal'
alias couldron="cd /home/pactrag/Programme/couldron/"
alias which='command -v'
alias pacse='pacman --sync --list --quiet | grep'
alias pacup='sudo pacman -Syu'
alias pacrm='sudo pacman --remove --cascade --nosave --recursive'
alias pacin='sudo pacman -S'
alias uller='
[ -d /mnt/windows/Windows ] ||
	su -c "
	[ -d /mnt/windows ] || mkdir /mnt/windows
	mount /dev/disk/by-uuid/9E8C4B068C4AD903 /mnt/windows
	"
cd /mnt/windows/Users/Uller
'
alias manRan="man -w --regex . | shuf -n 1 | xargs man"
alias clipboard='nvim /home/pactrag/Programme/sysinfo/misc/clipboard.txt +$'
alias fajita_usb="adb forward tcp:8022 tcp:8022 && ssh localhost -p 8022"
# I did not use $2 instead of the regex pattern because I do not want to
# trigger shellcheck [SC2142].
alias shell_scripts_running="
ps -eo pid=,command= | awk '(/\/bin\/sh/){ print }'
"
alias nixc="su -p -c 'nvim /etc/nixos/configuration.nix'"
alias nixco="su -p -c 'nixos-rebuild switch'"
alias nixcon="su -p -c 'nixos-rebuild switch --upgrade'"
alias nixh="nvim -p /home/pactrag/.config/nixpkgs/*.nix"
alias nvimStart="cd /home/pactrag/.local/share/nvim/site/pack/default/start"
alias nixho="home-manager switch"
alias nixman="nvim -R -n \"+tab Man configuration.nix(5) | -tabclose\""
alias poweroff="printf 'use turnoff\n'"
alias reboot="printf 'use restart\n'"
alias refresh="su -p -c 'nixos-rebuild switch --upgrade'"
alias nvimSplit="tmux split-window -h nvim"
alias restart=" /home/pactrag/Programme/usw/shell/some_shutdown_stuff.sh &&
systemctl reboot"
alias turnoff="/home/pactrag/Programme/usw/shell/some_shutdown_stuff.sh &&
systemctl poweroff"
alias xop="xsel --input --clipboard"
alias clear="clear; tmux clear-history"
alias down="aria2c --no-conf"
alias fview="fzf --preview 'cat {}'"
alias gitload="git commit -a -m '‏‏‎' && git push -u origin master"
alias gitlogrm="git log --diff-filter=D --summary | grep delete"
alias gitrm="git rm --cached"
alias mpvn="mpv --no-resume-playback --save-position-on-quit=no"
alias net_scan="fping -a -g 192.168.1.101 192.168.1.146 | tr '\n' ' '"
alias netspeed="curl -L -o /dev/null https://tinyurl.com/yk2fzuhe"
alias rmempty="find . -maxdepth 1 -empty -delete"
alias modat="find . -type f -exec stat --format '%y ~> %n' '{}' \+"
alias perm='stat -c "%n | %a | %A"'
alias systemd='cd /home/pactrag/.config/systemd/user/'
# escaping quotes is hell
alias xon='perl -e '\''
ioctl STDOUT, 0x5412, $_ for split //, do{ chomp($_ = <>); $_ }; print "\n"
'\'''
# alias tinyurl="curl -s  -w '\n' tinyurl.com/api-create.php?url="
# alias zlng="curl -w '\n' cheat.sh/"
alias ls="ls --color=always"
alias t='nvim scp://user@${TERMUX}:8022//data/data/com.termux/files/home/misc/todo.txt'

# LS_COLORS
. /home/pactrag/.config/dircolors/ls_colors

# I am putting these functions here because there is no way to cd into a
# directory using a shell script unless I sourced it or ran exec, but
# a flawed
fajita_mtp() {
	# cd to another directory so that fusermount will not complain if
	# I am in the mounted directory
	cd /
	mount_point=/home/pactrag/.mount/fajita/mtp
	if [ -d '/home/pactrag/.mount/fajita/mtp/Internal shared storage' ]; then
		fusermount -u "${mount_point}" >/dev/null 2>&1
	fi

	set +m
	/home/pactrag/Programme/bin/go-mtpfs "${mount_point}" >/dev/null 2>&1 &
	set -m
	# do NOT remove the sleep command.
	sleep 0.1
	cd '/home/pactrag/.mount/fajita/mtp/Internal shared storage' || return
}

fajita_sshfs() {
	if [ -d /home/pactrag/.mount/fajita/sshfs/0/Android ]; then
		fusermount -u /home/pactrag/.mount/fajita/sshfs/0 >/dev/null 2>&1
	fi
	sshfs user@"${TERMUX-}":/storage/emulated/0/ \
		/home/pactrag/.mount/fajita/sshfs/0 -p 8022 \
		-o cache=yes \
		-o kernel_cache \
		-o compression=no \
		-o Ciphers=aes128-ctr

	if [ -d /home/pactrag/.mount/fajita/sshfs/home/.ssh ]; then
		fusermount -u /home/pactrag/.mount/fajita/sshfs/home >/dev/null 2>&1
	fi
	sshfs user@"${TERMUX}":/data/data/com.termux/files/home/ \
		/home/pactrag/.mount/fajita/sshfs/home -p 8022 \
		-o cache=yes \
		-o kernel_cache \
		-o compression=no \
		-o Ciphers=aes128-ctr

	cd /home/pactrag/.mount/fajita/sshfs || return
}

# Prevent trailing new lines from running commands (comes into effect when
# pasting stuff).
bind 'set enable-bracketed-paste on'

tabs -4

PS1="% "

if [ -z "${TMUX-}" ]; then
	if [ -n "${XTERM_DROPDOWN-}" ]; then
		exec tmux new -A -s least
	elif [ "${TERM}" = 'xterm-256color' ]; then
		exec tmux new -A -s main
	else
		exec tmux new -A -s teletypewriter
	fi
fi

PATH="/home/pactrag/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/pactrag/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/pactrag/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/pactrag/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/pactrag/perl5"; export PERL_MM_OPT;
