#!/bin/sh

# exports
# I do not use Ruby, never
export BROWSER='firefox'
export EDITOR='nvim'
export VISUAL='nvim'
# export POSIXLY_CORRECT='1'
export PATH="/home/pactrag/Programme/usw/shell/override:\
/home/pactrag/Programme/usw/shell/inpath:\
/home/pactrag/Programme/usw/shell/override:\
/home/pactrag/Programme/usw/shell/inpath/no_arg/:\
${PATH}"
export PAGER='nvim -R -n +Man!'
# export PAGER=less
# export MANPAGER=less
export TERMUX='192.168.2.103'
export NPM_CONFIG_USERCONFIG='/home/pactrag/.config/npm/npmrc'
export GNUPGHOME='/home/pactrag/.local/share/gnupg'
export PROMPT_COMMAND='history -a'
export TEXMFHOME='/home/pactrag/.local/share/texmf'
export ANDROID_PREFS_ROOT='/home/pactrag/.local/share/android'
export TEXMFVAR='/home/pactrag/.cache/texlive/texmf-var'
export TEXMFCONFIG='/home/pactrag/.config/texlive/texmf-config'
export XDG_DOCUMENTS_DIR='/home/pactrag/Downloads/Documents'
export XDG_DOWNLOAD_DIR='/home/pactrag/Downloads'
export XDG_MUSIC_DIR='/home/pactrag/Downloads/Music'
export XDG_PICTURES_DIR='/home/pactrag/Downloads/Pictures'
export XDG_VIDEOS_DIR='/home/pactrag/Downloads/Videos'

export HISTFILE='/home/pactrag/.cache/bash_history'
[ -f /home/pactrag/.bash_history ] && rm /home/pactrag/.bash_history

bind -x '"\C-l": /home/pactrag/Programme/usw/shell/bash_bindings.sh clear'
bind -x '"\C-r": /home/pactrag/Programme/usw/shell/bash_bindings.sh history'

if [ -z "${DISPLAY}" ] && [ "$(tty)" = '/dev/tty1' ]; then
	exec startx
fi

# source ~/.bashrc (Arch did by default in the original .profile and Tmux
# needs it anyway)
# Source .bashrc only if in an interactive shell (tty -s returns 0 if
# interactive)
# tty --silent && [ -f /home/pactrag/.bashrc ] && . /home/pactrag/.bashrc

# Checking if ${PS1} exists is more efficient.
[ -n "${PS1-}" ] && [ -f /home/pactrag/.bashrc ] && . /home/pactrag/.bashrc
